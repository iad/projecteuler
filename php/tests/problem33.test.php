<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem33Test extends PHPUnit_Framework_TestCase
{
    public function testIsCuriousFraction() {
        $problemSolver = new \Problem\ProblemSolver33();
        $result = $problemSolver->isCuriousFraction(49, 98);
        $this->assertEquals(true, $result);
    }

    public function testIsCuriousFraction2() {
        $problemSolver = new \Problem\ProblemSolver33();
        $result = $problemSolver->isCuriousFraction(49, 97);
        $this->assertEquals(false, $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver33();
        $result = $problemSolver->solve();
        echo "result = $result \n";
    }
}

