<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem13Test extends PHPUnit_Framework_TestCase
{
    public function testSafeSum1() {
        $str1 = "111111111111111111111111111111111111111111111111";
        $str2 = "222222222222222222222222222222222222222222222222";
        $str3 = "666666666666666666666666666666666666666666666667";
        $goal = "1000000000000000000000000000000000000000000000000";
        $result = \Problem\Problem13::solve($data = [$str1, $str2, $str3]);
        $this->assertEquals($goal, $result);
    }

    public function testDefaultCase()
    {
        echo 'result= ' . \Problem\Problem13::solve(file("./data/problem13.txt", FILE_IGNORE_NEW_LINES)) . "\n";
    }
}


