<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem4Test extends PHPUnit_Framework_TestCase
{
    public function testIsPolindrome1()
    {
        $result = \Problem\Problem4::isPolindrome(9001);
        $this->assertEquals($result, false);
    }

    public function testIsPolindrome2()
    {
        $result = \Problem\Problem4::isPolindrome(9009);
        $this->assertEquals($result, true);
    }
}


//echo 'polindrome=' . \Problem\Problem4::solve() . "\n";

