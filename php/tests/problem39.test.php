<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem39Test extends PHPUnit_Framework_TestCase
{
    public function testGetC() {
        $problemSolver = new \Problem\ProblemSolver39();
        $result = $problemSolver->getC(20, 48);
        $this->assertEquals(52, $result);
    }

    public function testGetNumberOfSolutions() {
        $problemSolver = new \Problem\ProblemSolver39();
        $result = $problemSolver->getNumberOfSolutions(120);
        $this->assertEquals(3, $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver39();
        $result = $problemSolver->solve(1000);
        echo "result = $result \n";
    }
}
