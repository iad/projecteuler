<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem29Test extends PHPUnit_Framework_TestCase
{
    public function testSolve() {
        $problemSolver = new \Problem\ProblemSolver29();
        $result = $problemSolver->solve(5, 5);
        $this->assertEquals(15, $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver29();
        $result = $problemSolver->solve(100, 100);
        echo "result = $result \n";
    }
}
