<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem32Test extends PHPUnit_Framework_TestCase
{
    public function testIsPandigital() {
        $problemSolver = new \Problem\ProblemSolver32();
        $result = $problemSolver->isPandigital(15234);
        $this->assertEquals(true, $result);
    }

    public function testIsPandigital2() {
        $problemSolver = new \Problem\ProblemSolver32();
        $result = $problemSolver->isPandigital(15233);
        $this->assertEquals(false, $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver32();
        $result = $problemSolver->solve();
        echo "result = $result \n";
    }
}

