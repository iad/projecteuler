<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem67Test extends PHPUnit_Framework_TestCase
{
    public function testCase1() {
        $data = [
            [3],
            [7, 4],
            [2, 4, 6],
            [8, 5, 9, 3]
        ];

        $problemSolver = new \Problem\ProblemSolver67();
        $result = $problemSolver->solve($data);
        $this->assertEquals(23, $result);
    }

    public function testDefaultCase()
    {
        $data = [];
        $lines = file(__DIR__ . "/data/p067_triangle.txt", FILE_IGNORE_NEW_LINES);

        foreach ($lines as $line) {
            $row = explode(' ', $line);
            $data[] = $row;
        }

        $problemSolver = new \Problem\ProblemSolver67();
        $result = $problemSolver->solve($data);
        echo "result = $result \n";
    }
}


