<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem23Test extends PHPUnit_Framework_TestCase
{
    public function testGetAbundantNumbers() {
        $problemSolver = new \Problem\ProblemSolver23();
        $result = $problemSolver->getAbundantNumbers(12);

        $this->assertEquals([12], $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver23();
        $result = $problemSolver->solve(28123);
        echo "result = $result \n";
    }
}


