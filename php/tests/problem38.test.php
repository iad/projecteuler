<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem38Test extends PHPUnit_Framework_TestCase
{
    public function testGetPandigital() {
        $problemSolver = new \Problem\ProblemSolver38();
        $result = $problemSolver->getPandigital(192, 3);
        $this->assertEquals('192384576', $result);
    }

    public function testIs9DigitNumber() {
        $problemSolver = new \Problem\ProblemSolver38();
        $result = $problemSolver->is9DigitNumber('192384576');
        $this->assertEquals(true, $result);
    }

    public function testIs9DigitNumber2() {
        $problemSolver = new \Problem\ProblemSolver38();
        $result = $problemSolver->is9DigitNumber('192384575');
        $this->assertEquals(false, $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver38();
        $result = $problemSolver->solve();
        echo "result = $result \n";
    }
}
