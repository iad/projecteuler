<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem43Test extends PHPUnit_Framework_TestCase
{
    public function testHasInterestingProperty()
    {
        $problemSolver = new \Problem\ProblemSolver43();
        $result = $problemSolver->hasInterestingProperty('1406357289');
        $this->assertEquals(true, $result);
    }

    public function testHasInterestingProperty2()
    {
        $problemSolver = new \Problem\ProblemSolver43();
        $result = $problemSolver->hasInterestingProperty('1406357298');
        $this->assertEquals(false, $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver43();
        $result = $problemSolver->solve();
        echo "result = $result \n";
    }
}
