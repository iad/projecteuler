<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem22Test extends PHPUnit_Framework_TestCase
{
    public function testParseString()
    {
        $problemSolver = new \Problem\ProblemSolver22();
        $result = $problemSolver->parseString('"test1", "test2"');
        $this->assertEquals(['test1', 'test2'], $result);
    }

    public function testSortList()
    {
        $problemSolver = new \Problem\ProblemSolver22();
        $result = $problemSolver->sortList(["lemon", "orange", "banana", "apple"]);
        $this->assertEquals(['apple', 'banana', 'lemon', 'orange'], $result);
    }

    public function testGetScore()
    {
        $problemSolver = new \Problem\ProblemSolver22();
        $result = $problemSolver->getScore('COLIN');
        $this->assertEquals(53, $result);
    }

    public function testFileParse() {
        $problemSolver = new \Problem\ProblemSolver22();

        $text = file_get_contents( __DIR__ . "/data/p022_names.txt");
        $list = $problemSolver->parseString($text);
        $sortedList = $problemSolver->sortList($list);
        $this->assertEquals('COLIN', $sortedList[938 - 1]);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver22();

        $text = file_get_contents( __DIR__ . "/data/p022_names.txt");
        $result = $problemSolver->solve($text);
        echo "result = $result \n";
    }
}


