<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem12Test extends PHPUnit_Framework_TestCase
{
    public function testGetCountOfDivisors1() {
        $result = \Problem\Problem12::getCountOfDivisors(6);
        $this->assertEquals(4, $result);
    }

    public function testGetCountOfDivisors2() {
        $result = \Problem\Problem12::getCountOfDivisors(28);
        $this->assertEquals(6, $result);
    }

    public function testGetCountOfDivisors3() {
        $result = \Problem\Problem12::getCountOfDivisors(3);
        $this->assertEquals(2, $result);
    }

    public function testDefaultCase()
    {
        $result = \Problem\Problem12::solve(4);
        $this->assertEquals(6, $result);
    }
}

//echo 'result=' . \Problem\Problem12::solve(500) . "\n";

