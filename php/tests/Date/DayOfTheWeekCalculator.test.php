<?php

require_once __DIR__.'/../../vendor/autoload.php';

class DayOfTheWeekCalculatorTest extends PHPUnit_Framework_TestCase
{
    public function testGetNumberOfDaysSince1900To() {
        $calculator = new \Date\DayOfTheWeekCalculator();
        $this->assertEquals(0, $calculator->getNumberOfDaysSince1900To(1900, 1, 1));
    }

    public function testGetNumberOfDaysSince1900To2() {
        $calculator = new \Date\DayOfTheWeekCalculator();
        $this->assertEquals(366, $calculator->getNumberOfDaysSince1900To(1901, 1, 2));
    }

    public function testGetNumberOfDaysInYear() {
        $calculator = new \Date\DayOfTheWeekCalculator();
        $this->assertEquals(365, $calculator->getNumberOfDaysInYear(1900));
    }

    public function testGetNumberOfDaysInYear2() {
        $calculator = new \Date\DayOfTheWeekCalculator();
        $this->assertEquals(365, $calculator->getNumberOfDaysInYear(1901));
    }

    public function testGetNumberOfDaysInYear2000() {
        $calculator = new \Date\DayOfTheWeekCalculator();
        $this->assertEquals(366, $calculator->getNumberOfDaysInYear(2000));
    }

    public function testGetDayOfWeek()
    {
        $calculator = new \Date\DayOfTheWeekCalculator();
        $this->assertEquals(1, $calculator->getDayOfWeek(1900, 1, 1));
    }

    public function testGetDayOfWeekForRealDate()
    {
        $calculator = new \Date\DayOfTheWeekCalculator();
        $this->assertEquals(7, $calculator->getDayOfWeek(2015, 12, 13));
    }

}



