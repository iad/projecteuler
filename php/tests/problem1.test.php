<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem1Test extends PHPUnit_Framework_TestCase
{
    public function testDefaultCase()
    {
        $result = \Problem\Problem1::solve(10);
        $this->assertEquals($result, 23);
    }
}


//echo '1000=' . \Problem\Problem1::solve(1000) . "\n";

