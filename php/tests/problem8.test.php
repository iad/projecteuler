<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem8Test extends PHPUnit_Framework_TestCase
{

    public function testGetProduct()
    {
        $result = \Problem\Problem8::getProduct('9989');
        $this->assertEquals($result, 5832);
    }

    public function testDefaultCase()
    {
        $result = \Problem\Problem8::solve(4);
        $this->assertEquals($result, 5832);
    }
}

//echo 'result=' . \Problem\Problem8::solve(13) . "\n";

