<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem27Test extends PHPUnit_Framework_TestCase
{
    public function testGetLengthOfPrimeNumbersSequence() {
        $problemSolver = new \Problem\ProblemSolver27();
        $generator = new \Math\PrimeNumbersGenerator();

        $primeNumbers = $generator->getPrimeNumbers(2000000);
        $result = $problemSolver->getLengthOfPrimeNumbersSequence(1, 41, $primeNumbers);
        $this->assertEquals(40, $result);
    }

    public function testGetLengthOfPrimeNumbersSequence2() {
        $problemSolver = new \Problem\ProblemSolver27();
        $generator = new \Math\PrimeNumbersGenerator();

        $primeNumbers = $generator->getPrimeNumbers(2000000);
        $result = $problemSolver->getLengthOfPrimeNumbersSequence(-79, 1601, $primeNumbers);
        $this->assertEquals(80, $result);
    }

    public function testSolve2()
    {
        $problemSolver = new \Problem\ProblemSolver27();
        $result = $problemSolver->solve(41);
        $this->assertEquals(-41, $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver27();
        $result = $problemSolver->solve(1000);
        echo "result = $result \n";
    }
}


