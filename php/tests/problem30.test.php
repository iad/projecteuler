<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem30Test extends PHPUnit_Framework_TestCase
{
    public function testGetMaximumPossibleNumber() {
        $problemSolver = new \Problem\ProblemSolver30();
        $result = $problemSolver->getMaximumPossibleNumber(5);
        $this->assertEquals(354294, $result);
    }

    public function testGetSumOfPowers() {
        $problemSolver = new \Problem\ProblemSolver30();
        $result = $problemSolver->getSumOfPowers(1634, 4);
        $this->assertEquals(1634, $result);
    }

    public function testPower() {
        $problemSolver = new \Problem\ProblemSolver30();
        $result = $problemSolver->power(3, 3);
        $this->assertEquals(27, $result);
    }


    public function testGetPoweredNumbers() {
        $problemSolver = new \Problem\ProblemSolver30();
        $result = $problemSolver->getPoweredNumbers(4);
        $this->assertEquals([1634, 8208, 9474], $result);
    }

    public function testSolve() {
        $problemSolver = new \Problem\ProblemSolver30();
        $result = $problemSolver->solve(4);
        $this->assertEquals(19316, $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver30();
        $result = $problemSolver->solve(5);
        echo "result = $result \n";
    }
}
