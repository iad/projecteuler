<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem25Test extends PHPUnit_Framework_TestCase
{
    public function testParseString()
    {
        $problemSolver = new \Problem\ProblemSolver25();
        $result = $problemSolver->solve(3);
        $this->assertEquals('12', $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver25();
        $result = $problemSolver->solve(1000);
        echo "result = $result \n";
    }
}


