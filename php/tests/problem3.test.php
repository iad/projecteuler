<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem3Test extends PHPUnit_Framework_TestCase
{
    public function testPrimeFactor()
    {
        $result = \Problem\Problem3::getPrimeFactor(25);
        $this->assertEquals($result, 5);
    }

    public function testPrimeFactor3()
    {
        $result = \Problem\Problem3::getPrimeFactor(27);
        $this->assertEquals($result, 3);
    }

    public function testPrimeFactors()
    {
        $result = \Problem\Problem3::getPrimeFactors(13195);
        $this->assertEquals($result, [5, 7, 13, 29]);
    }

    public function testDefaultCase()
    {
        $result = \Problem\Problem3::solve(13195);
        $this->assertEquals($result, 29);
    }
}


//echo '600851475143=' . \Problem\Problem3::solve(600851475143) . "\n";

