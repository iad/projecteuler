<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem10Test extends PHPUnit_Framework_TestCase
{
    public function testDefaultCase()
    {
        $result = \Problem\Problem10::solve(10);
        $this->assertEquals(17, $result);
    }
}

//echo 'result=' . \Problem\Problem10::solve(2000000) . "\n";

