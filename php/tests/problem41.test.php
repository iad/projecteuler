<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem41Test extends PHPUnit_Framework_TestCase
{
    public function testSolve() {
        $problemSolver = new \Problem\ProblemSolver41();
        $result = $problemSolver->solve(2144);
        $this->assertEquals(2143, $result);
    }
}

$problemSolver = new \Problem\ProblemSolver41();
$result = $problemSolver->solve(1000000000);
echo "result = $result \n";
