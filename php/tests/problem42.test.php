<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem42Test extends PHPUnit_Framework_TestCase
{
    public function testGetSumWord() {
        $problemSolver = new \Problem\ProblemSolver42();
        $result = $problemSolver->getSumWord('SKY');
        $this->assertEquals(55, $result);
    }

    public function testParseString()
    {
        $problemSolver = new \Problem\ProblemSolver42();
        $result = $problemSolver->parseString('"test1", "test2"');
        $this->assertEquals(['test1', 'test2'], $result);
    }

    public function testGetTriangularNumbers()
    {
        $problemSolver = new \Problem\ProblemSolver42();
        $result = $problemSolver->getTriangularNumbers(55);
        $this->assertEquals([1, 3, 6, 10, 15, 21, 28, 36, 45, 55], $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver42();

        $text = file_get_contents( __DIR__ . "/data/p042_words.txt");
        $result = $problemSolver->solve($text);
        echo "result = $result \n";
    }
}
