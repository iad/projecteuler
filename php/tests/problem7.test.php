<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem7Test extends PHPUnit_Framework_TestCase
{

    public function testIsPrimeNumber()
    {
        $result = \Problem\Problem7::isPrimeNumber(13);
        $this->assertEquals($result, true);
    }

    public function testIsNotPrimeNumber()
    {
        $result = \Problem\Problem7::isPrimeNumber(12);
        $this->assertEquals($result, false);
    }

    public function testDefaultCase()
    {
        $result = \Problem\Problem7::solve(6);
        $this->assertEquals($result, 13);
    }
}

//echo 'result=' . \Problem\Problem7::solve(10001) . "\n";

