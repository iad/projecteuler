<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem14Test extends PHPUnit_Framework_TestCase
{
    public function testCase1() {
        $problemSolver = new \Problem\ProblemSolver14();
        $result = $problemSolver->getCount(13);
        $this->assertEquals(10, $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver14();
        $result = $problemSolver->solve(1000000);
        echo "result = $result \n";
    }
}


