<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem20Test extends PHPUnit_Framework_TestCase
{
    public function testCase1() {
        $problemSolver = new \Problem\ProblemSolver20();
        $result = $problemSolver->solve(10);
        $this->assertEquals('27', $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver20();
        $result = $problemSolver->solve(100);
        echo "result = $result \n";
    }
}


