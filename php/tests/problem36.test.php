<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem36Test extends PHPUnit_Framework_TestCase
{
    public function testConvertToBinary() {
        $problemSolver = new \Problem\ProblemSolver36();
        $result = $problemSolver->convertToBinary(585);
        $this->assertEquals('1001001001', $result);
    }

    public function testConvertToBinary2() {
        $problemSolver = new \Problem\ProblemSolver36();
        $result = $problemSolver->convertToBinary(0);
        $this->assertEquals('0', $result);
    }

    public function testConvertToBinary3() {
        $problemSolver = new \Problem\ProblemSolver36();
        $result = $problemSolver->convertToBinary(1);
        $this->assertEquals('1', $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver36();
        $result = $problemSolver->solve(1000000);
        echo "result = $result \n";
    }
}

