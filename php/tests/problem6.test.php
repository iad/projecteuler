<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem6Test extends PHPUnit_Framework_TestCase
{

    public function testSumOfSquares()
    {
        $result = \Problem\Problem6::getSumOfSquares(10);
        $this->assertEquals($result, 385);
    }

    public function testSquareOfSum()
    {
        $result = \Problem\Problem6::getSquareOfSum(10);
        $this->assertEquals($result, 3025);
    }


    public function testDefaultCase()
    {
        $result = \Problem\Problem6::solve(10);
        $this->assertEquals($result, 2640);
    }
}


//echo 'result=' . \Problem\Problem6::solve(100) . "\n";

