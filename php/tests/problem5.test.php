<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem5Test extends PHPUnit_Framework_TestCase
{

    public function testDefaultCase()
    {
        $result = \Problem\Problem5::solve(10);
        $this->assertEquals($result, 2520);
    }
}


//echo 'smallest positive number that is evenly divisible by all of the numbers from 1 to 20=' . \Problem\Problem5::solve(20) . "\n";

