<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem15Test extends PHPUnit_Framework_TestCase
{
    public function testCase1() {
        $problemSolver = new \Problem\ProblemSolver15();
        $result = $problemSolver->solve(2,2);
        $this->assertEquals(6, $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver15();
        $result = $problemSolver->solve(20, 20);
        echo "result = $result \n";
    }
}


