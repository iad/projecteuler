<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem16Test extends PHPUnit_Framework_TestCase
{
    public function testCase1() {
        $problemSolver = new \Problem\ProblemSolver16();
        $result = $problemSolver->solve(15);
        $this->assertEquals(26, $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver16();
        $result = $problemSolver->solve(1000);
        echo "result = $result \n";
    }
}


