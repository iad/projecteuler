<?php

require_once __DIR__.'/../../vendor/autoload.php';

class NodeTest extends PHPUnit_Framework_TestCase
{
    public function testGetTotalValue()
    {
        $parentNode = new \Graph\Node(1);
        $childNode1 = new \Graph\Node(2);
        $childNode2 = new \Graph\Node(3);

        $parentNode->addChild($childNode1);
        $parentNode->addChild($childNode2);
        $this->assertEquals(6, $parentNode->getTotalValue());
    }

    public function testGetTotalValueDifficult()
    {
        $parentNode = new \Graph\Node(1);
        $childNode1 = new \Graph\Node(2);
        $childNode2 = new \Graph\Node(3);

        $childNode21 = new \Graph\Node(4);

        $parentNode->addChild($childNode1);
        $parentNode->addChild($childNode2);

        $parentNode->getTotalValue();

        $childNode2->addChild($childNode21);
        $this->assertEquals(10, $parentNode->getTotalValue());
    }

    public function testGetMinimalTotalTreeValue() {
        $parentNode = new \Graph\Node(1);
        $childNode1 = new \Graph\Node(2);
        $childNode2 = new \Graph\Node(3);

        $childNode21 = new \Graph\Node(4);

        $parentNode->addChild($childNode1);
        $parentNode->addChild($childNode2);

        $parentNode->getTotalValue();

        $childNode2->addChild($childNode21);
        $this->assertEquals(3, $parentNode->getMinimalTotalTreeValue());
    }

    public function testGetMaxTotalTreeValue() {
        $parentNode = new \Graph\Node(1);
        $childNode1 = new \Graph\Node(2);
        $childNode2 = new \Graph\Node(3);

        $childNode21 = new \Graph\Node(4);

        $parentNode->addChild($childNode1);
        $parentNode->addChild($childNode2);

        $parentNode->getTotalValue();

        $childNode2->addChild($childNode21);
        $this->assertEquals(8, $parentNode->getMaximalTotalTreeValue());
    }


}



