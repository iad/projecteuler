<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem35Test extends PHPUnit_Framework_TestCase
{
    public function testGetPermutations() {
        $problemSolver = new \Problem\ProblemSolver35();
        $result = $problemSolver->getPermutations('123');
        $this->assertEquals([123, 132, 213, 231, 312, 321], $result);
    }

    public function testGetRotations() {
        $problemSolver = new \Problem\ProblemSolver35();
        $result = $problemSolver->getRotations('123');
        $this->assertEquals([123, 231, 312], $result);
    }

    public function testGetPermutations2() {
        $problemSolver = new \Problem\ProblemSolver35();
        $result = $problemSolver->getPermutations('53');
        $this->assertEquals([53, 35], $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver35();
        $result = $problemSolver->solve(1000000);
        echo "result = $result \n";
    }
}

