<?php

require_once __DIR__.'/../../vendor/autoload.php';

class LettersPermutatorTest extends PHPUnit_Framework_TestCase
{
    public function testGetPermutations() {
        $permutator = new \Text\LettersPermutator();
        $result = $permutator->getPermutations('123');
        $this->assertEquals([123, 132, 213, 231, 312, 321], $result);
    }

    public function testGetPermutations2() {
        $permutator = new \Text\LettersPermutator();
        $result = $permutator->getPermutations('53');
        $this->assertEquals([53, 35], $result);
    }
}

