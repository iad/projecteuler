<?php

require_once __DIR__.'/../../vendor/autoload.php';

class BigNumberTest extends PHPUnit_Framework_TestCase
{
    public function testAdd()
    {
        $number = new \Math\BigNumber('11111111111111111111111111111111111111111111');
        $number->add('2222222222');
        $this->assertEquals('11111111111111111111111111111111113333333333', (string)$number);
    }

    public function testAddDifficult()
    {
        $number = new \Math\BigNumber('9999999999999999999999999999999999999999999');
        $number->add('9999999999999999999999999999999999999999999');
        $this->assertEquals('19999999999999999999999999999999999999999998', (string)$number);
    }

    public function testInvolution1() {
        $number = new \Math\BigNumber('2');
        $number->involution(3);
        $this->assertEquals('8', (string)$number);
    }

    public function testInvolution2() {
        $number = new \Math\BigNumber('2');
        $number->involution(0);
        $this->assertEquals('1', (string)$number);
    }

    public function testInvolution3() {
        $number = new \Math\BigNumber('3');
        $number->involution(3);
        $this->assertEquals('27', (string)$number);
    }

    public function testInvolution4() {
        $number = new \Math\BigNumber('2');
        $number->involution(20);
        $this->assertEquals('1048576', (string)$number);
    }

    public function testMultiply() {
        $number = new \Math\BigNumber(3);
        $number->multiply(4);
        $this->assertEquals('12', (string)$number);
    }

    public function testMultiply2() {
        $number = new \Math\BigNumber(321);
        $number->multiply(4321);
        $this->assertEquals('1387041', (string)$number);
    }


    public function testMultiply3() {
        $number = new \Math\BigNumber(131072);
        $number->multiply(2);
        $this->assertEquals('262144', (string)$number);
    }

    public function testFactorial() {
        $number = new \Math\BigNumber(3);
        $number->factorial();
        $this->assertEquals('6', (string)$number);
    }

    public function testFactorial2() {
        $number = new \Math\BigNumber(10);
        $number->factorial();
        $this->assertEquals('3628800', (string)$number);
    }

    public function testGetSumOfDivisors() {
        $number = new \Math\BigNumber(220);
        $this->assertEquals('284', $number->getSumOfDivisors());
    }

    public function testGetSumOfDivisors2() {
        $number = new \Math\BigNumber(284);
        $this->assertEquals('220', $number->getSumOfDivisors());
    }

    public function testIsAbundant() {
        $number = new \Math\BigNumber(12);
        $this->assertEquals(true, $number->isAbundant());
    }

    public function testIsAbundant2() {
        $number = new \Math\BigNumber(13);
        $this->assertEquals(false, $number->isAbundant());
    }

    public function testIsPalindrome() {
        $number = new \Math\BigNumber(56765);
        $this->assertEquals(true, $number->isPalindrome());
    }

    public function testIsPalindrome2() {
        $number = new \Math\BigNumber(567651);
        $this->assertEquals(false, $number->isPalindrome());
    }

    public function testIsPandigital() {
        $number = new \Math\BigNumber(2143);
        $this->assertEquals(true, $number->isPandigital());
    }

    public function testIsPandigital2() {
        $number = new \Math\BigNumber(2145);
        $this->assertEquals(false, $number->isPandigital());
    }

    public function testIsPandigital3() {
        $number = new \Math\BigNumber(1406357289);
        $this->assertEquals(true, $number->isPandigital());
    }

}



