<?php

require_once __DIR__.'/../../vendor/autoload.php';

class PrimeNumbersGeneratorTest extends PHPUnit_Framework_TestCase
{
    public function testGetPrimeNumbers() {
        $generator = new \Math\PrimeNumbersGenerator();
        $result = $generator->getPrimeNumbers(20);
        $this->assertEquals([2 => true, 3 => true, 5 => true, 7 => true, 11 => true, 13 => true, 17 => true, 19 => true], $result);
    }

    public function testGetPrimeNumbers2() {
        $generator = new \Math\PrimeNumbersGenerator();
        $result = $generator->getPrimeNumbers(20, 10);
        $this->assertEquals([11 => true, 13 => true, 17 => true, 19 => true], $result);
    }
}