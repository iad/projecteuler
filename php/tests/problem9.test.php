<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem9Test extends PHPUnit_Framework_TestCase
{
    public function testDefaultCase()
    {
        $result = \Problem\Problem9::solve(12);
        $this->assertEquals($result, 60);
    }
}

//echo 'result=' . \Problem\Problem9::solve(1000) . "\n";

