<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem34Test extends PHPUnit_Framework_TestCase
{
    public function testIsCuriousNumber() {
        $problemSolver = new \Problem\ProblemSolver34();
        $result = $problemSolver->isCuriousNumber(145);
        $this->assertEquals(true, $result);
    }

    public function testIsCuriousNumber2() {
        $problemSolver = new \Problem\ProblemSolver34();
        $result = $problemSolver->isCuriousNumber(146);
        $this->assertEquals(false, $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver34();
        $result = $problemSolver->solve();
        echo "result = $result \n";
    }
}

