<?php

require_once __DIR__.'/../vendor/autoload.php';

class Problem37Test extends PHPUnit_Framework_TestCase
{
    public function testGetTruncatedNumbers() {
        $problemSolver = new \Problem\ProblemSolver37();
        $result = $problemSolver->getTruncatedNumbers(3797);
        $this->assertEquals(['797', '97', '7', '3', '37', '379'], $result);
    }

    public function testDefaultCase()
    {
        $problemSolver = new \Problem\ProblemSolver37();
        $result = $problemSolver->solve();
        echo "result = $result \n";
    }
}

