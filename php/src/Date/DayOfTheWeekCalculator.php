<?php
/**
 * Created by IntelliJ IDEA.
 * User: igor
 * Date: 11.12.15
 * Time: 3:45
 */

namespace Date;


class DayOfTheWeekCalculator
{

    public function getNumberOfDaysSince1900To($targetYear, $targetMonth, $targetDay) {
        $days = 0;
        for ($year = 1900; $year < $targetYear; $year++) {
            $days += $this->getNumberOfDaysInYear($year);
        }
        for ($month = 1; $month < $targetMonth; $month++) {
            $days += $this->getNumberOfDaysInMonth($year, $month);
        }
        $days += $targetDay - 1;
        return $days;
    }

    public function getNumberOfDaysInYear($year) {
        $days = 0;
        for($i = 1; $i<=12; $i++) {
            $days += $this->getNumberOfDaysInMonth($year, $i);
        }
        return $days;
    }

    public function getNumberOfDaysInMonth($year, $month) {
        $days = 31;
        if ($month == 9 || $month == 4 || $month == 11 || $month == 6) {
            $days = 30;
        }
        if ($month == 2) {
            if (($year % 4 ) == 0) {
                if (($year % 400) == 0 || ($year % 100) != 0) {
                    $days = 29;
                } else {
                    $days = 28;
                }
            } else {
                $days = 28;
            }
        }
        return $days;
    }

    public function getDayOfWeek($year, $month, $day) {
        $dayOfWeek = ($this->getNumberOfDaysSince1900To($year, $month, $day) + 1) % 7;
        $dayOfWeek = ($dayOfWeek == 0) ? $dayOfWeek + 7 : $dayOfWeek;
        return $dayOfWeek;
    }
}