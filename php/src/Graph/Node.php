<?php

namespace Graph;

class Node
{
    private $value = 0;

    /**
     * @var Node
     */
    private $parent = null;

    /**
     * @var Node[]
     */
    private $children = [];

    /**
     * @var int
     */
    private $cachedTotalValue = null;

    /**
     * @param int
     */
    private $cachedMinimalChildValue = null;

    /**
     * @param int
     */
    private $cachedMaximalChildValue = null;

    public function __construct($value = 0)
    {
        $this->value = $value;
    }

    public function __toString() {
        return (string)$this->value;
    }

    public function setParent(Node $node) {
        if ($node != $this->parent) {
            $this->parent = $node;
            $node->addChild($this);
        }
    }

    public function addChild(Node $node) {
        if (!in_array($node, $this->children, true)) {
            $this->children[] = $node;
            $node->setParent($this);
            $this->invalidateCache();
        }
    }

    public function invalidateCache() {
        $this->cachedTotalValue = null;
        $this->cachedMinimalChildValue = null;
        $this->cachedMaximalChildValue = null;

        if ($this->parent) {
            $this->parent->invalidateCache();
        }
    }

    public function getTotalValue() {
        if (!is_null($this->cachedTotalValue)) {
            return $this->cachedTotalValue;
        }

        $totalValue = $this->value;

        foreach($this->children as $child) {
            $totalValue += $child->getTotalValue();
        }

        $this->cachedTotalValue = $totalValue;

        return $totalValue;
    }

    public function getMinimalTotalTreeValue() {
        if ($this->cachedMinimalChildValue) {
            return $this->cachedMinimalChildValue;
        }

        $minimalChildValue = null;
        foreach($this->children as $child) {
            if (is_null($minimalChildValue)) {
                $minimalChildValue = $child->getMinimalTotalTreeValue();
            } else {
                if (($value = $child->getMinimalTotalTreeValue()) < $minimalChildValue) {
                    $minimalChildValue = $value;
                }
            }
        }

        $this->cachedMinimalChildValue = $minimalChildValue;

        return $minimalChildValue + $this->value;
    }

    public function getMaximalTotalTreeValue() {
        if ($this->cachedMaximalChildValue) {
            return $this->cachedMaximalChildValue + $this->value;
        }

        $maxChildValue = null;
        foreach($this->children as $child) {
            if (is_null($maxChildValue)) {
                $maxChildValue = $child->getMaximalTotalTreeValue();
            } else {
                if (($value = $child->getMaximalTotalTreeValue()) > $maxChildValue) {
                    $maxChildValue = $value;
                }
            }
        }

        $this->cachedMaximalChildValue = $maxChildValue;

        return $maxChildValue + $this->value;
    }

}