<?php

namespace Problem;

use Math\BigNumber;

class ProblemSolver42
{
    public function getTriangularNumbers($max) {
        $result = [];
        $n = 1;
        while (true) {
            $number = $n * ($n + 1) / 2;
            if ($number > $max) break;
            $result[] = $number;
            $n++;
        }
        return $result;
    }

    public function getSumWord($word) {
        $sum = 0;
        for($i = 0 ; $i < strlen($word); $i++) {
            $sum += (ord($word[$i]) - ord('A') + 1);
        }
        return $sum;
    }

    public function parseString($line) {
        $list = explode(",", $line);
        foreach($list as &$item) {
            $item = str_replace('"', '', $item);
            $item = str_replace(' ', '', $item);
        }
        return $list;
    }

    public function solve($line)
    {
        $triangularNumbers = $this->getTriangularNumbers(10000);
        $result = [];

        foreach ($this->parseString($line) as $word) {
            if (in_array($this->getSumWord($word), $triangularNumbers)) {
                $result[] = $word;
                echo "w=$word \n";
            }
        }
        return count($result);
    }
}
