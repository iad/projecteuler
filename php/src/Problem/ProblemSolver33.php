<?php

namespace Problem;

class ProblemSolver33
{
    public function isCuriousFraction($a, $b) {
        $digit = floor($a / 10);

        $a1 = str_replace($digit, '', $a);
        $b1 = str_replace($digit, '', $b);
        if ((int)$digit !=0 && (int)$b1 != 0 && ((int)$a / (int)$b) == ((int)$a1 / (int)$b1)) {
            return true;
        }

        $digit = $a % 10;

        $a1 = str_replace($digit, '', $a);
        $b1 = str_replace($digit, '', $b);
        if ((int)$digit !=0 && (int)$b1 != 0 && ((int)$a / (int)$b) == ((int)$a1 / (int)$b1)) {
            return true;
        }
        return false;
    }

    public function getLowestCuriousFractionDenominator($a, $b) {
        $digit = floor($a / 10);

        $a1 = str_replace($digit, '', $a);
        $b1 = str_replace($digit, '', $b);
        if ((int)$digit !=0 && (int)$b1 != 0 && ((int)$a / (int)$b) == ((int)$a1 / (int)$b1)) {
            return 1/ ($a1/ $b1);
        }

        $digit = $a % 10;

        $a1 = str_replace($digit, '', $a);
        $b1 = str_replace($digit, '', $b);
        if ((int)$digit !=0 && (int)$b1 != 0 && ((int)$a / (int)$b) == ((int)$a1 / (int)$b1)) {
            return 1/ ($a1/ $b1);
        }
        return false;
    }

    public function solve()
    {
        $product = 1;
        for ($i = 10; $i<99; $i++) {
            for($j = $i + 1; $j<99; $j++) {
                if ($this->isCuriousFraction($i, $j)) {
                    $product *= $this->getLowestCuriousFractionDenominator($i, $j);
                }
            }
        }
        return $product;
    }
}
