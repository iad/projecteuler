<?php

namespace Problem;

class Problem6
{
    public static function getSumOfSquares($number) {
        $result = 0;
        for ($i = 1; $i<=$number; $i++) {
            $result += $i * $i;
        }
        return $result;
    }

    public static function getSquareOfSum($number) {
        $result = 0;
        for ($i = 1; $i<=$number; $i++) {
            $result += $i;
        }
        return $result * $result;
    }

    public static function solve($number)
    {
        return self::getSquareOfSum($number) - self::getSumOfSquares($number);
    }
}
