<?php

namespace Problem;

class Problem12
{
    public function getCountOfDivisors($number)
    {
        $k = 0;
        $i = 2;
        $total = 1;
        while(($number % $i) == 0)
        {
            $number /= $i;
            $k++;
        }
        $total = $total * ++$k;
        $i++;
        $k = 0;
        while($number != 1)
        {
            if($i * $i > $number) $i = $number;
            while(($number % $i) == 0)
            {
                $number /= $i;
                $k++;
            }
            $total *= ++$k;
            $k = 0;
            $i += 2;
        }
        return $total;
    }

    public static function getNumberByPosition($position) {
        return ($position * ($position + 1)) / 2;
    }

    public static function solve($targetCount)
    {
        $position = 1;
        while (true) {
            $number = self::getNumberByPosition($position);
            $count = self::getCountOfDivisors($number);
            if ($count >= $targetCount) {
                return $number;
            } else {
                $position++;
            }
        }
    }
}
