<?php

namespace Problem;

use Math\BigNumber;
use Math\PrimeNumbersGenerator;

class ProblemSolver41
{
    public function solve($maxNumber)
    {
        $generator = new PrimeNumbersGenerator();
        $result = 0;

        $split = 1000000;
        for ($s=0; $s <= floor($maxNumber / $split); $s++) {
            $min = $s * $split;
            $max = $min + $split;
            if ($max>$maxNumber) $max = $maxNumber;
            $primeNumbers = $generator->getPrimeNumbers($max, $min);
            foreach(array_keys($primeNumbers) as $primeNumber) {
                $number = new BigNumber($primeNumber);
                if ($number->isPandigital()) {
                    echo "$primeNumber \n";
                    $result = $primeNumber;
                }
            }

        }
        return $result;
    }
}
