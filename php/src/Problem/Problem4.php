<?php

namespace Problem;

class Problem4
{
    public static function isPolindrome($number) {
        return (strrev((string)$number) === (string)$number);
    }

    public static function solve()
    {
        $polindrome = 0;
        for ($i = 100; $i<=999; $i++) {
            for ($j = 100; $j<=999; $j++) {
                $number = $i * $j;
                if (self::isPolindrome($number)) {
                    if ($number > $polindrome) {
                        $polindrome = $number;
                    }
                }
            }
        }

        return $polindrome;
    }
}
