<?php

namespace Problem;

use Graph\Node;

class ProblemSolver15
{
    public function solve($maxX, $maxY)
    {
        $nodes = [];

        for ($x = 0; $x <=$maxX; $x++) {
            for ($y = 0; $y<=$maxY; $y++) {
                if (!isset($nodes[$x])) {
                    $nodes[$x] = [];
                }

                $value = 0;
                if ($x == $maxX - 1 && $y == $maxY) {
                    $value = 1;
                }
                if ($y == $maxY - 1 && $x == $maxX) {
                    $value = 1;
                }

                $node = new Node($value);
                if ($x>0) {
                    $node->setParent($nodes[$x-1][$y]);
                }
                if ($y>0) {
                    $node->setParent($nodes[$x][$y-1]);
                }

                $nodes[$x][$y] = $node;
            }
        }

        $node = $nodes[0][0];
        /** @var $node Node */
        return $node->getTotalValue();
    }
}
