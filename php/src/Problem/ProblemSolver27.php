<?php

namespace Problem;

use Math\BigNumber;

class ProblemSolver27
{
    public function getLengthOfPrimeNumbersSequence($a, $b, $primeNumbers) {
        $count = 0;
        $n = 0;
        while (true) {
            $number = $n * $n + $a * $n + $b;
            if (!empty($primeNumbers[$number])) {
                $count++;
                $n++;
            } else {
                return $count;
            }
        }
    }

    public function solve($digits)
    {
        $generator = new \Math\PrimeNumbersGenerator();
        $primeNumbers = $generator->getPrimeNumbers(2000000);
        $productAB = 0;
        $maxSequence = 0;
        for ($a = -$digits; $a<= $digits; $a++) {
            for ($b = -$digits; $b<=$digits; $b++) {
                $sequenceLength = $this->getLengthOfPrimeNumbersSequence($a, $b, $primeNumbers);
                if ($sequenceLength > $maxSequence) {
                    $maxSequence = $sequenceLength;
                    $productAB = $a * $b;
                }
            }
        }

        return $productAB;
    }
}
