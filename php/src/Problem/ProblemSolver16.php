<?php

namespace Problem;

use Math\BigNumber;

class ProblemSolver16
{
    public function solve($step)
    {
        $number = new BigNumber(2);
        $number->involution($step);

        $targetNumber = (string)$number;
        $result = 0;
        for ($i=0; $i<strlen((string)$targetNumber); $i++) {
            $result += (int)$targetNumber[$i];
        }

        return $result;
    }
}
