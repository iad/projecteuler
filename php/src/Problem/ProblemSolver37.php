<?php

namespace Problem;

use Math\PrimeNumbersGenerator;

class ProblemSolver37
{
    public function getTruncatedNumbers($number) {
        $number = (string)$number;
        $numbers = [];
        for ($i = 1; $i < strlen($number); $i++) {
            $numbers[] = substr($number, $i);
        }
        for ($i = 1; $i < strlen($number); $i++) {
            $numbers[] = substr($number, 0, $i);
        }
        return $numbers;
    }

    public function solve()
    {
        $sum = 0;
        $count = 0;
        $generator = new PrimeNumbersGenerator();
        $primeNumbers = $generator->getPrimeNumbers(1000000);
        foreach(array_keys($primeNumbers) as $primeNumber) {
            if ($primeNumber < 10) continue;
            $truncatedNumbers = $this->getTruncatedNumbers($primeNumber);
            foreach($truncatedNumbers as $truncatedNumber) {
                if (empty($primeNumbers[$truncatedNumber])) {
                    continue 2;
                }
            }
            $sum += $primeNumber;
            $count ++;
            if ($count >= 11) break;
        }
        return $sum;
    }
}
