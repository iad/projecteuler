<?php

namespace Problem;

use Math\BigNumber;
use Symfony\Component\Yaml\Tests\B;

class ProblemSolver36
{
    public function convertToBinary($number) {
        $result = '';
        $step = 0;
        while (true) {
            $n = new BigNumber(2);
            $n->involution($step);
            if ($n->toInteger() > $number) {
                break;
            }
            $step++;
        }
        for ($i = $step - 1; $i>=0; $i--) {
            $n = new BigNumber(2);
            $n->involution($i);
            if ($number >= $n->toInteger()) {
                $number -= $n->toInteger();
                $result .= '1';
            } else {
                $result .= '0';
            }
        }
        if ($result == '') $result = '0';
        return $result;
    }

    public function solve($max)
    {
        $sum = 0;
        for ($i = 1; $i<$max; $i++) {
            $number = new BigNumber($i);
            if ($number->isPalindrome()) {
                $binaryNumber = new BigNumber($this->convertToBinary($i));
                if ($binaryNumber->isPalindrome()) {
                    echo "$i, $binaryNumber \n";
                    $sum += $i;
                }
            }
        }
        return $sum;
    }
}
