<?php

namespace Problem;

class Problem9
{

    public static function solve($number)
    {
        for ($i = 1; $i < ($number -2); $i++) {
            for ($j = $i + 1; $j < ($number - 1); $j++) {
                $k = $number - $i - $j;
                if (($i < $j) && ($j < $k) && (($i * $i + $j * $j) == ($k * $k))) {
                    return $i * $j * $k;
                }
            }
        }
        return false;
    }
}
