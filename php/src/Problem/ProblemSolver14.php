<?php

namespace Problem;

class ProblemSolver14
{
    private $countsCache = [];

    public function getCount($number) {
        if (isset($this->countsCache[$number])) {
            $result = $this->countsCache[$number];
        } elseif ($number == 1) {
            $result = 1;
        } elseif (($number % 2) == 0) {
            $result = $this->getCount($number / 2) + 1;
            $this->countsCache[$number] = $result;
        } else {
            $result = $this->getCount(3 * $number + 1) +1;
            $this->countsCache[$number] = $result;
        }

        return $result;
    }


    public function solve($number)
    {
        $maxCount = 0;
        $maxCountNumber = 0;
        for ($i = 1; $i <= $number; $i ++) {
            $count = $this->getCount($i);
            if ($count > $maxCount) {
                $maxCount = $count;
                $maxCountNumber = $i;
            }
        }
        return $maxCountNumber;
    }
}
