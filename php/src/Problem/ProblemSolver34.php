<?php

namespace Problem;

use Math\BigNumber;

class ProblemSolver34
{
    public function isCuriousNumber($number) {
        $curious = 0;
        $number = (string)$number;
        for ($i = 0; $i<strlen($number); $i++) {
            $currentNumber = new BigNumber($number[$i]);
            $currentNumber->factorial();
            $curious += (int)(string)$currentNumber;
        }
        return $number == $curious;

    }

    public function solve()
    {
        $sum = 0;
        for($i =3; $i<100000; $i++) {
            if ($this->isCuriousNumber($i)) {
                $sum += $i;
            }
        }
        return $sum;
    }
}
