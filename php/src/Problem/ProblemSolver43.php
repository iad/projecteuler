<?php

namespace Problem;

use Math\BigNumber;

class ProblemSolver43
{
    public function hasInterestingProperty($number) {
        $number = (string)$number;

        if (strlen($number) != 10) {
            return false;
        }

        $divs = [
            2 => 2,
            3 => 3,
            4 => 5,
            5 => 7,
            6 => 11,
            7 => 13,
            8 => 17
        ];

        for ($i = 2; $i<=8; $i++) {
            $partOfNumber = substr($number, $i -1, 3);
            if ((int)$partOfNumber % $divs[$i]) {
                return false;
            }
        }

        return true;
    }

    public function solve()
    {
        $permutator = new \Text\LettersPermutator();
        $permutations = $permutator->getPermutations('1234567890');

        $sum = 0;
        foreach ($permutations as $permutation) {
            $number = new BigNumber($permutation);
            if ($number->isPandigital() && $this->hasInterestingProperty($permutation)) {
                $sum += (int)$permutation;
            }
        }

        return $sum;
    }
}
