<?php

namespace Problem;

use Math\BigNumber;

class ProblemSolver21
{
    public function solve($maxNumber)
    {
        $count = 0;

        for ($i=1; $i<=$maxNumber; $i++) {
            $number = new BigNumber($i);
            $sumOfDivisors = $number->getSumOfDivisors();

            if ($sumOfDivisors < $maxNumber && $sumOfDivisors != $i) {
                if ((new BigNumber($sumOfDivisors))->getSumOfDivisors() == $i) {
                    $count += $i;
                }
            }
        }

        return $count;
    }
}
