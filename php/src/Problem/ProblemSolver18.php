<?php

namespace Problem;

use Graph\Node;

class ProblemSolver18
{
    public function solve($data)
    {
        $nodes = [];

        foreach($data as $rowIndex=>$row) {
            foreach($row as $colIndex=>$value) {
                $node = new Node((int)$value);

                if (empty($data[$rowIndex])) {
                    $data[$rowIndex] = [];
                }

                $nodes[$rowIndex][$colIndex] = $node;

                if (!empty($nodes[$rowIndex-1][$colIndex])) {
                    $node->setParent($nodes[$rowIndex-1][$colIndex]);
                }
                if (!empty($nodes[$rowIndex-1][$colIndex-1])) {
                    $node->setParent($nodes[$rowIndex-1][$colIndex-1]);
                }
            }
        }

        /* @var $mainNode Node */
        $mainNode = $nodes[0][0];

        var_dump($nodes[0][0]->getMaximalTotalTreeValue());

        return $mainNode->getMaximalTotalTreeValue();
    }
}
