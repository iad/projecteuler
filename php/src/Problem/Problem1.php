<?php

namespace Problem;

class Problem1
{
    public static function solve($number)
    {
        $sum = 0;
        for($i = 1; $i < $number; $i++) {
            if (self::isMultiple($i, 3) | self::isMultiple($i, 5)) {
                $sum += $i;
            }
        }
        return $sum;
    }

    private static function isMultiple($number, $basis)
    {
        return ($number % $basis) === 0;
    }
}
