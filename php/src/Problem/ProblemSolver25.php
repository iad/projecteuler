<?php

namespace Problem;

use Math\BigNumber;

class ProblemSolver25
{
    public function solve($digits)
    {
        $prev1 = '1';
        $prev2 = '1';
        $count = 3;
        while (true) {
            $number = new BigNumber($prev2);
            $number->add($prev1);
            if (strlen((string)$number) >= $digits) {
                return $count;
            } else {
                $prev1 = $prev2;
                $prev2 = $number;
                $count++;
            }
        }
    }
}
