<?php

namespace Problem;

class Problem10
{
    public static function solve($number)
    {
        $map = [];
        for ($i = 3; $i <= $number; $i = $i + 2) {
            for ($j = ($i * $i); $j <= $number; $j = $j + $i) {
                // mark as not prime
                $map[$j] = false;
            }
        }

        $sum = 2;
        for ($i = 3; $i < $number; $i = $i + 2) {
            if (!isset($map[$i])) {
                $sum += $i;
            }
        }

        return $sum;
    }
}
