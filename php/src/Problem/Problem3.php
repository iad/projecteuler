<?php

namespace Problem;

class Problem3
{
    public static function getPrimeFactors($number)
    {
        $primeFactors = [];

        while (($primeFactor = self::getPrimeFactor($number)) !== 1) {
            $primeFactors[] = $primeFactor;
            $number = $number / $primeFactor;
        }
        return $primeFactors;
    }

    public static function getPrimeFactor($number)
    {
        for ($i = 3; $i <= $number/2; $i = $i + 2) {
            if (($number % $i) === 0) {
                return $i;
            }
        }
        return $number;
    }

    public static function solve($number)
    {
        return max(self::getPrimeFactors($number));
    }
}
