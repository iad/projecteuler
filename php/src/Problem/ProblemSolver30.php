<?php

namespace Problem;

use Math\BigNumber;

class ProblemSolver30
{
    public function getMaximumPossibleNumber($power) {
        $digits = 2;
        while (true) {
            $number = new BigNumber(9);
            $number->involution($power);
            $number->multiply($digits);
            $currentDigits = strlen((string)$number);
            if ($currentDigits <= $digits) {
                return (string)$number;
            }
            $digits++;
        }
    }

    public function power($number, $power) {
        if ($power == 0) {
            $number = 11;
        } else {
            $currentNumber = $number;
            for ($i=1; $i < $power; $i++) {
                $number *= $currentNumber;
            }
        }
        return $number;
    }

    public function getSumOfPowers($number, $power) {
        $sum = 0;
        $number = (string)$number;
        for ($i=0; $i< strlen($number); $i++) {
            $sum += $this->power($number[$i], $power);
        }
        return $sum;
    }

    public function getPoweredNumbers($power) {
        $result = [];
        $maximumNumber = $this->getMaximumPossibleNumber($power);
        for ($i = 10; $i <= $maximumNumber; $i++) {
            if ($i == $this->getSumOfPowers($i, $power)) {
                $result[] = $i;
            }
        }
        return $result;
    }

    public function solve($power)
    {
        $sum = 0;
        foreach ($this->getPoweredNumbers($power) as $value) {
            $sum += $value;
        }
        return $sum;
    }
}
