<?php

namespace Problem;

use Math\BigNumber;

class ProblemSolver22
{
    public function parseString($line) {
        $list = explode(",", $line);
        foreach($list as &$item) {
            $item = str_replace('"', '', $item);
            $item = str_replace(' ', '', $item);
        }
        return $list;
    }

    public function sortList($list) {
        sort($list);
        return $list;
    }

    public function getScore($text) {
        $score = 0;
        for ($i = 0; $i<strlen($text); $i++) {
            $score += ord(strtolower($text[$i])) - ord('a') + 1;
        }
        return $score;
    }

    public function solve($text)
    {
        $list = $this->parseString($text);
        $sortedList = $this->sortList($list);

        $score = 0;
        foreach($sortedList as $index=>$value) {
            $score += $this->getScore($value) * ($index+1);
        }

        return $score;
    }
}
