<?php

namespace Problem;

use Math\BigNumber;

class ProblemSolver29
{
    public function getCombinations($a, $b) {
        $map = [];
        for ($i = 2; $i<=$a; $i++) {
            $number = new BigNumber($i);
            for ($j = 2; $j<=$b; $j++) {
                $number->multiply($i);
                $map[(string)$number] = true;
            }
        }
        return array_keys($map);
    }

    public function solve($a, $b)
    {
        $combinations = $this->getCombinations($a, $b);
        return count($combinations);
    }
}
