<?php

namespace Problem;

use Date\Day;
use Date\DayOfTheWeekCalculator;
use Math\BigNumber;

class ProblemSolver20
{
    public function solve($test)
    {
        $number = new BigNumber($test);
        $number->factorial();

        $count = 0;
        $value = (string)$number;
        for ($i = 0; $i<strlen($value); $i++) {
            $count += $value[$i];
        }

        return $count;
    }
}
