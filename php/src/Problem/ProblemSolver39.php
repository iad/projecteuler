<?php

namespace Problem;

class ProblemSolver39
{
    public function getC($a, $b) {
        return sqrt($a * $a + $b * $b);
    }

    public function getNumberOfSolutions($p) {
        $count = 0;
        for ($a=1; $a < ($p -2); $a++) {
            for($b = $a + 1; $b < ($p - $a - 1); $b++) {
                $c = $this->getC($a, $b);
                if ($c == floor($c) && ($a + $b + $c) == $p && $c > $b) {
                    $count++;
                }
            }
        }
        return $count;
    }

    public function solve($p)
    {
        $maxNumberOfSolutions = 0;
        $pForMaxNumberOfSolutinos = 0;
        for ($i = 1; $i <= $p; $i++) {
            $numberOfSolutions = $this->getNumberOfSolutions($i);
            if ($numberOfSolutions > $maxNumberOfSolutions) {
                $maxNumberOfSolutions = $numberOfSolutions;
                $pForMaxNumberOfSolutinos = $i;
            }
        }
        return $pForMaxNumberOfSolutinos;
    }
}
