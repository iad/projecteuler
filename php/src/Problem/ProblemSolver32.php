<?php

namespace Problem;

use Math\BigNumber;

class ProblemSolver32
{
    public function isPandigital($number) {
        $number = (string)$number;
        $counters = [];
        for($i = 0; $i<strlen($number); $i++) {
            $digit = (int)$number[$i];
            if (empty($counters[$digit])) {
                $counters[$digit] = 0;
            }
            $counters[$digit] ++;
        }
        if (!empty($counters[0])) {
            return false;
        }
        for($i=1; $i<=strlen($number); $i++) {
            if (empty($counters[$i])) {
                return false;
            }
            if ($counters[$i] != 1) {
                return false;
            }
        }
        return true;
    }

    public function solve()
    {
        $products = [];
        for ($i = 1; $i<=100; $i++) {
            for ($j = $i + 1; $j<=999999999; $j++) {
                $number = new BigNumber($i);
                $number->multiply($j);
                $concantenatedNumbers = (string)$i . (string)$j . (string)$number;
                if (strlen($concantenatedNumbers) > 9) {
                    continue 2;
                }
                if (strlen($concantenatedNumbers) == 9 && $this->isPandigital($concantenatedNumbers)) {
                    $products[(string)$number] = true;
                }
            }
        }
        return array_sum(array_keys($products));
    }
}
