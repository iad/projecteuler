<?php

namespace Problem;

use Date\Day;
use Date\DayOfTheWeekCalculator;

class ProblemSolver19
{
    public function solve()
    {
        $calculator = new DayOfTheWeekCalculator();
        $sundays = 0;

        for ($year = 1901; $year <=2000; $year++) {
            for($month = 1; $month<= 12; $month++) {
                if ($calculator->getDayOfWeek($year, $month, 1) == 7) {
                    $sundays++;
                }
            }
        }
        return $sundays;
    }
}
