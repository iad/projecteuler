<?php

namespace Problem;

class Problem7
{
    public static function isPrimeNumber($number) {
        if (($number % 2) ===0) return false;

        for ($i = 3; $i <= $number/2; $i = $i + 2) {
            if (($number % $i) === 0) {
                return false;
            }
        }
        return true;
    }

    public static function solve($targetPosition)
    {
        $position = 0;
        $number = 0;
        while($position < $targetPosition) {
            $number++;
            if (self::isPrimeNumber($number)) {
                $position++;
            }
        }
        return $number;
    }
}
