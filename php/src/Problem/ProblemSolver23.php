<?php

namespace Problem;

use Math\BigNumber;

class ProblemSolver23
{
    public function getAbundantNumbers($highNumber) {
        $numbers = [];
        for($i = 1; $i<=$highNumber; $i++) {
            $number = new BigNumber($i);
            if ($number->isAbundant()) {
                $numbers[] = $i;
            }
        }
        return $numbers;
    }

    public function solve($highNumber)
    {
        $abundantNumbers = $this->getAbundantNumbers($highNumber);

        $sum = 0;
        $excludedNumbers = [];

        for($n1 = 0; $n1<count($abundantNumbers); $n1++) {
            for($n2= $n1 ; $n2<count($abundantNumbers); $n2++) {
                $excludedNumbers[$abundantNumbers[$n1] + $abundantNumbers[$n2]] = true;
            }
        }

        for ($i = 1; $i <= $highNumber; $i++) {
            if (empty($excludedNumbers[$i])) {
                $sum += $i;
                echo "n= $i \n";
            }
        }
        return $sum;
    }
}
