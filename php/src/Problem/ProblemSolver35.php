<?php

namespace Problem;

use Math\PrimeNumbersGenerator;

class ProblemSolver35
{
    public function getPermutations($str) {
        $str = (string)$str;
        $results = [];
        if (strlen($str) == 1) {
            return [$str];
        }
        for($i = 0; $i<strlen($str); $i++) {
            $cutString = substr($str, 0, $i) . substr($str, $i + 1);
            if ($cutString) {
                $permutations = $this->getPermutations($cutString);
                foreach($permutations as $permutation) {
                    $results[] = $str[$i] . $permutation;
                }
            }
        }
        return $results;

    }

    public function getRotations($str) {
        $rotations = [];
        $str = (string)$str;
        for($i = 0; $i < strlen($str); $i++) {
            $rotations[] =  substr($str, $i) . substr($str, 0, $i);
        }
        return $rotations;
    }

    public function solve($max)
    {
        $funNumbers = [];
        $generator = new PrimeNumbersGenerator();
        $primeNumbers = $generator->getPrimeNumbers($max);
        foreach(array_keys($primeNumbers) as $number) {
            if ($number == 1) {
                continue;
            }
            $permutations = $this->getRotations($number);
            foreach ($permutations as $permutation) {
                if (empty($primeNumbers[(int)$permutation])) {
                    continue 2;
                }
            }
            $funNumbers[] = $number;
        }
        return count($funNumbers);
    }
}
