<?php

namespace Problem;

class ProblemSolver38
{
    public function getPandigital($number, $n) {
        $result = '';
        for ($i = 1; $i<=$n; $i++) {
            $result .= (string) ($number * $i);
        }
        return $result;
    }

    public function is9DigitNumber($number) {
        $number = (string)$number;
        if (strlen($number) != 9) {
            return false;
        }
        for ($i=1; $i<=9; $i++) {
            if (strpos($number, (string)$i) === false) {
                return false;
            }
        }
        return true;
    }

    public function solve()
    {
        $maxPandigital = 0;

        for ($i = 1; $i <100000; $i++) {
            $n = 2;
            while(true) {
                $number = $this->getPandigital($i, $n);
                if(strlen($number) < 9) {
                    $n++;
                    continue;
                }
                if ($this->is9DigitNumber($number)) {
                    $maxPandigital = ((int)$number > (int)$maxPandigital) ? $number : $maxPandigital;
                    $n++;
                    continue;
                }
                if(strlen($number) > 9) {
                    continue 2;
                }
                $n++;
            }
        }

        return $maxPandigital;
    }
}
