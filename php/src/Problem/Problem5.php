<?php

namespace Problem;

class Problem5
{
    public static function isEvenlyDivisible($number, $basis) {
        for ($i = 2; $i<$basis; $i++) {
            if (($number % $i) !== 0) {
                return false;
            }
        }
        return true;
    }

    public static function solve($step)
    {
        $number = $step;
        while (! self::isEvenlyDivisible($number, $step)) {
            $number += $step;
        }
        return $number;
    }
}
