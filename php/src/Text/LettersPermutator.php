<?php

namespace Text;

class LettersPermutator
{
    public function getPermutations($str) {
        $str = (string)$str;
        $results = [];
        if (strlen($str) == 1) {
            return [$str];
        }
        for($i = 0; $i<strlen($str); $i++) {
            $cutString = substr($str, 0, $i) . substr($str, $i + 1);
            if ($cutString) {
                $permutations = $this->getPermutations($cutString);
                foreach($permutations as $permutation) {
                    $results[] = $str[$i] . $permutation;
                }
            }
        }
        return $results;
    }
}