<?php

namespace Math;

class BigNumber
{
    private $number= '';

    public function __construct($number = '')
    {
        $this->number = (string)$number;
    }

    public function __toString() {
        return (string)$this->number;
    }

    public function add($number)
    {
        $digitsCount = max(strlen($this->number), strlen((string)$number));
        $number1_rev = strrev((string)$this->number);
        $number2_rev = strrev((string)$number);

        $result_rev = '';
        $add = '';
        for($i=0; $i<$digitsCount; $i++) {
            $digit = (int)substr($number1_rev, $i, 1) + (int)substr($number2_rev, $i, 1) + (int)$add;
            $add = (int)($digit > 9);
            $digit = ($digit % 10);

            $result_rev .= (string)$digit;
        }
        $result_rev .= (string)$add ? (string)$add : '';

        return $this->number = strrev($result_rev);
    }

    public function multiply($number) {
        $digitsCount = max(strlen($this->number), strlen((string)$number));
        $number1_rev = strrev((string)$this->number);
        $number2_rev = strrev((string)$number);

        $digits = [];
        $result_rev = '';
        for($i = 0; $i < $digitsCount; $i++) {
            if (empty($digits[$i])) {
                $digits[$i] = 0;
            }
            for($j = 0; $j < $digitsCount; $j++) {
                $d1 = (int)substr($number1_rev, $i, 1);
                $d2 = (int)substr($number2_rev, $j, 1);

                $result = $d1 * $d2;
                if (empty($digits[$i + $j])) {
                    $digits[$i + $j] = 0;
                }
                $digits[$i + $j] += $result;
            }
        }

        for ($i = 0; $i<count($digits); $i++) {
            $count = $digits[$i];
            $add = floor((int)$count / 10);
            $digits[$i] = ((int)$count % 10);
            if ($add > 0) {
                if (empty($digits[$i + 1])) {
                    $digits[$i + 1] = 0;
                }
                $digits[$i + 1] += $add;
            }
        }

        foreach($digits as $digit) {
            $result_rev .= (string)$digit;
        }

        $result = strrev($result_rev);
        while(true) {
            if ($result[0] == 0) {
                $result = substr($result, 1);
            } else {
                $this->number = $result;
                break;
            }
        }
    }

    public function factorial() {
        $fact = $this->number;
        $this->number = 1;;
        for ($i = 1; $i<= $fact; $i++) {
            $this->multiply($i);
        }
    }

    public function involution($step) {
        if ($step == 0) {
            $this->number = '1';
        } else {
            $number = $this->number;
            for ($i=1; $i<$step; $i++) {
                $this->multiply($number);
            }
        }
    }

    public function getSumOfDivisors() {
        $sum = 0;
        for ($i = 1; $i <= ($this->number / 2); $i++) {
            if (($this->number % $i) == 0) {
                $sum += $i;
            }
        }

        return $sum;
    }

    public function toInteger() {
        return (int)$this->number;
    }

    public function isAbundant() {
        return ($this->getSumOfDivisors() > $this->number);
    }

    public function isPalindrome() {
        return (strrev((string)$this->number) === (string)$this->number);
    }

    public function isPandigital() {
        for ($i=1; $i<=strlen($this->number); $i++) {
            $digit = (string)$i;
            if ($i == 10) $digit = '0';

            if (strpos($this->number, $digit) === false) {
                return false;
            }
        }
        return true;
    }
}