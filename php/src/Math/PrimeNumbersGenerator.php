<?php

namespace Math;

class PrimeNumbersGenerator
{
    public function getPrimeNumbers($max, $min = 0) {
        $notPrimeNumbers = [];
        $primeNumbers = [];

        if ($min < 3) $primeNumbers[2] = true;
        if ($min < 3) $min = 3;
        if (!($min % 2)) $min++;

        for ($i = 3; $i <= $max; $i = $i + 2) {
            $start = floor($min / $i) * $i;
            if ($start < ($i * $i)) $start = $i * $i;

            for ($j = $start; $j <= $max; $j = $j + $i) {
                // mark as not prime
                $notPrimeNumbers[$j] = true;
            }
        }

        for ($i = $min; $i < $max; $i = $i + 2) {
            if (!isset($notPrimeNumbers[$i])) {
                $primeNumbers[$i] = true;
            }
        }

        return $primeNumbers;
    }
}