'use strict';

var exports = module.exports = {};

exports.compareLettersCount = function (str1, str2) {
  return (str1.toString().split("").sort().join("") == str2.toString().split("").sort().join(""))
};

exports.checkNumber = function (num) {
  return exports.compareLettersCount(num, 2 * num) &&
    exports.compareLettersCount(num, 3 * num) &&
    exports.compareLettersCount(num, 4 * num) &&
    exports.compareLettersCount(num, 5 * num) &&
    exports.compareLettersCount(num, 6 * num);
};

exports.solve = function (callback) {
  var num = 10000;
  while (true) {
    if (exports.checkNumber(num)) {
      callback(null, num);
      break;
    }
    num++;
  }
};
