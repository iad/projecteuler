'use strict';

var exports = module.exports = {};
var bigInteger = require('big-integer');
var _ = require("lodash");

exports.isLychrelNumber = function (num) {
  var count = 1;
  var testNumber = bigInteger(num).add(exports.getReverseNumber(num));
  while (count < 50) {
    if (exports.isPalindromeNumber(testNumber)) {
      return false
    }
    testNumber = testNumber.add(exports.getReverseNumber(testNumber.toString()));
    count++
  }
  return true
};

exports.getReverseNumber = function(num) {
  return num.toString().split("").reverse().join("")
};

exports.isPalindromeNumber = function (num) {
  return (num.toString() == exports.getReverseNumber(num));
};

exports.solve = function() {
  return _.chain(_.range(1, 10001))
    .reduce(function(result, num) {
      return exports.isLychrelNumber(num) ? ++result : result;
    }, 0)
    .value();
};