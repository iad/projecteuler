'use strict';

var exports = module.exports = {};
var async = require('async');
var BigNumber = require('bignumber.js');

exports.solve = function (max) {
  var valueOnPositionMinus1 = 2;
  var valueOnPositionMinus2 = 1;
  var sum = new BigNumber(2);

  while (true) {
    var currentValue = valueOnPositionMinus1 + valueOnPositionMinus2;
    if (currentValue>max) break;

    if (currentValue % 2 == 0) {
      sum = sum.add(currentValue);
    }
    valueOnPositionMinus2 = valueOnPositionMinus1;
    valueOnPositionMinus1 = currentValue;
  }

  return sum.toString();
};
