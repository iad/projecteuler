'use strict';

var exports = module.exports = {};
var _ = require('lodash');

exports.getPermutations = function(str) {

  if (str.length == 1) {
    return [str];
  }

  var list = str.split("");
  var result = [];

  for (var i = 0; i < str.length; i++) {
    var char = str[i];
    var newStr = str.substr(0, i) + str.substr(i+1);
    var permutations = exports.getPermutations(newStr);
    for (var permutation of permutations) {
      result.push(char.concat(permutation))
    }
  }
  return _.unique(result);
};
