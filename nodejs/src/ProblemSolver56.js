'use strict';

var exports = module.exports = {};
var bigInteger = require('big-integer');
var _ = require('lodash');


exports.getPow = function (num, pow) {
  var result = bigInteger(num).pow(pow);
  return result.toString()
};

exports.getSumOfDigits = function(num) {
  return _.chain(num.toString().split(""))
    .reduce(function(total, digit) {
      total += parseInt(digit);
      return total;
    }, 0)
    .value();
};

exports.solve = function(size) {
  return _.chain(_.range(1, size + 1))
    .map(function(a) {
      return _.chain(_.range(1, size + 1))
        .map(function(b) {
          return exports.getSumOfDigits(exports.getPow(a, b));
        })
        .value();
    })
    .reduce(function(items, list) {
      return items.concat(list);
    }, [])
    .max()
    .value();
};