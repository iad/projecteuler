'use strict';

var exports = module.exports = {};
var _ = require('lodash');

exports.getFactorial = function (num) {
  if (num == 0) {
    return 1;
  }
  if (num == 1) {
    return 1;
  }

  return num * exports.getFactorial(num - 1);
};

exports.getSumFactorialsOfDigits = function (num) {
  var sum = _.chain(num.toString().split(""))
    .map(function (digit) {
      return exports.getFactorial(digit)
    })
    .reduce(function (total, value) {
      total += value;
      return total;
    }, 0)
  .value();

  return sum;
};

var digitFactorialChainCache = {};

exports.getDigitFactorialChain = function(num, previousChain) {
  if (!previousChain) {
    previousChain = [];
  }
  var currentChain = _.clone(previousChain);

  if (digitFactorialChainCache[num] != undefined) {
    return digitFactorialChainCache[num];
  }

  currentChain.push(num);

  var nextNumber = exports.getSumFactorialsOfDigits(num);
  if (currentChain.indexOf(nextNumber) !== -1) {
    return currentChain;
  }


  var digitFactorialChainForNextNumber = exports.getDigitFactorialChain(nextNumber, currentChain);
  var digitFactorialChain = _.uniq(currentChain.concat(digitFactorialChainForNextNumber));

  if (previousChain.length == 0) {
    digitFactorialChainCache[num] = digitFactorialChain;
  }
  return digitFactorialChain;
};

exports.solve = function (max) {
  var count = 0;

  for (var i of _.range(1, max+1)) {
    var chain = exports.getDigitFactorialChain(i);
    if (chain.length == 60) {
      count++;
    }
  }

  return count;
};

