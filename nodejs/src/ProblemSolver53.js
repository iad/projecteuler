'use strict';

var exports = module.exports = {};
var factorial = require('big-factorial');
var _ = require('lodash');

exports.getCountOfSelections = function (n, k) {
  return factorial(n).divide(factorial(k).multiply(factorial(n - k)))
};

exports.solve = function (max, gt) {

  return _.chain(_.range(1, max + 1))
    .map(function (n) {
      return _.chain(_.range(1, n))
        .map(function (k) {
          return exports.getCountOfSelections(n, k)
        })
        .value()
    })
    .reduce(function(result, n) {
      return result.concat(n);
    }, [])
    .filter(function(value) {
      return value.greater(gt);
    })
    .map(function(value) {
      return value.toString()
    })
    .value()
    .length;
};