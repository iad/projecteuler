'use strict';

var exports = module.exports = {};
var _ = require('lodash');
var BigNumber = require('bignumber.js');

exports.solve = function (max) {
  var numerator = 0;
  var immediatelyLeftNumber = new BigNumber(0);
  var targetNumber = (new BigNumber(3)).div(7);
  var n = new BigNumber(1);
  for (var d of _.range(1, max +1)) {
    while (true) {
      var num = n.div(d);
      if (num.greaterThan(targetNumber)) {
        break;
      }
      if (num.lessThan(targetNumber) && num.greaterThan(immediatelyLeftNumber)) {
        immediatelyLeftNumber = num;
        numerator = n;
      }
      n = n.plus(1);
    }
  }
  return numerator.toString();
};
