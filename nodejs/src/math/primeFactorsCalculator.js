'use strict';

var exports = module.exports = {};
var _ = require('lodash');
var BigNumber = require('bignumber.js');

var factorsCache = {};

exports.getFactors = function(number) {
  var result = [];

  if (factorsCache[number] != undefined) {
    return factorsCache[number];
  }

  var n = 2;
  while (n <= number) {
    if (number % n == 0) {
      result = [n].concat(exports.getFactors(number / n));
      result = _.uniq(result);
      break;
    } else {
      n++;
    }
  }

  factorsCache[number] = result;

  return result;
};

exports.getBothFactors = function(num1, num2) {
  return _.intersection(exports.getFactors(num1), exports.getFactors(num2));
};
