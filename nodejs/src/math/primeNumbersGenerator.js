'use strict';

var exports = module.exports = {};
var _ = require('lodash');

var gridWithNotPrimeNumbers = {1: true};
var maxKnownPrimeNumber = 0;

exports.isKnownPrimeNumber = function (num) {
  if (maxKnownPrimeNumber < num) {
    // force warm up cache
    exports.getPrimeNumbers(1, num);
    maxKnownPrimeNumber = num;
  }
  return (gridWithNotPrimeNumbers[num] == undefined)
};

exports.getPrimeNumbers = function (min, max) {
  var i = 2;
  while (i < max) {
    var j = i * 2;

    while (j <= max) {
      gridWithNotPrimeNumbers[j] = true;
      j += i;
    }
    i++;
    if (i % 2 == 0) {
      i++;
    }
  }

  var knownPrimeNumbers = [];
  for (i = min; i <= max; i++) {
    if (gridWithNotPrimeNumbers[i] == undefined) {
      knownPrimeNumbers.push(i);
    }
  }
  if (max>maxKnownPrimeNumber) {
    maxKnownPrimeNumber = max;
  }
  return knownPrimeNumbers;
};

