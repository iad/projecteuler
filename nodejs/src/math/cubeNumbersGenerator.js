'use strict';

var exports = module.exports = {};
var _ = require('lodash');

var gridWithCubeNumbers = {"1": true};
var maxKnownCubeNumber = 0;
var maxNumber = 0;

exports.isKnownCubeNumber = function (num) {
  if (maxKnownCubeNumber < num) {
    // force warm up cache
    console.log("warming up to ", num);
    exports.getCubeNumbers(1, num);
    maxKnownCubeNumber = num;
  }
  return (gridWithCubeNumbers[num.toString()] != undefined)
};

exports.getCubeNumber = function(num) {
  var cube =  num * num * num;
  gridWithCubeNumbers[cube.toString()] = true;
  return cube;
};

exports.getCubeNumbers = function (min, max) {
  var i = 1;

  var cubeNumbers = [];

  while (true) {
    var cubeNumber = exports.getCubeNumber(i);
    gridWithCubeNumbers[cubeNumber.toString()] = true;
    maxKnownCubeNumber = cubeNumber;
    maxNumber = i;

    if (cubeNumber >= min) {
      if (cubeNumber <= max) {
        cubeNumbers.push(cubeNumber);
      } else {
        return cubeNumbers
      }
    }
    i++;
  }
};

