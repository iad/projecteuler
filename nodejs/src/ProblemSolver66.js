'use strict';

var exports = module.exports = {};
var _ = require('lodash');
var BigNumber = require('bignumber.js');

//exports.getYForXAndD = function (x, d) {
//  x = new BigNumber(x);
//  return x.toPower(2).minus(1).div(d).squareRoot().toString();
//  //return Math.sqrt((x * x - 1) / d);
//};
//
//exports.isValidXForD = function (x, d) {
//  var y = new BigNumber(exports.getYForXAndD(x, d));
//  return y.isInteger();
//};
//
exports.findX = function (d) {
  var x = new BigNumber(2);
  var y = new BigNumber(1);

  while (true) {
    var x2 = x.toPower(2);
    var dy2 = y.toPower(2).mul(d);
    var dif = dy2.plus(1).minus(x2);

    if (dif.eq(0)) {
      return x;
    }
    if (dif.lessThan(0)) {
      y = y.plus(1);
    }
    if (dif.greaterThan(0)) {
      x = x.plus(1)
    }
  }
};

exports.isSquareNumber = function (num) {
  num = new BigNumber(num);
  return num.squareRoot().isInteger();
};

exports.solve = function (maxD) {
  var result = _.chain(_.range(2, maxD + 1))
    .filter(function (d) {
      return exports.isSquareNumber(d) == false
    })
    .map(function (d) {
      console.log(d);
      var x = exports.findX(d);
      console.log(d, x.toString());
      return {
        d: d,
        x: x
      };
    })
    .reduce(function (result, item) {
      if (item.x.greaterThan(result.x)) {
        result.x = item.x;
        result.d = item.d
      }
      return result;
    }, {x: 0, d: 0})
    .value();
  return result.d;
};
