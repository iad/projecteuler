'use strict';

var exports = module.exports = {};
var _ = require('lodash');

exports.getCombinations = function* (list, deep) {
  for (var value of list) {
    if (deep == 1) {
      yield [value];
    } else {
      var newList = _.filter(list, function (n) {
        return(n < value);
      });
      var getCombinations = exports.getCombinations(newList, deep - 1);

      var items;
      while (items = getCombinations.next().value) {
        var result = [value].concat(items);

        if (result.length == deep) {
          yield result;
        }
      }
    }
  }
};

exports.isMagicSet = function(set) {
  var getCombinations = exports.getCombinations(set, 2);
  var set;
  while(set = getCombinations.next().value) {
    var num1 = parseInt(set[0].toString() + set[1].toString());
    var num2 = parseInt(set[1].toString() + set[0].toString());
    if (!exports.isMagicPear(num1, num2)) {
      return false;
    }
  }
  return true;
};

exports.isMagicPear = function(num1, num2) {
  var primeNumbersGenerator = require('./math/primeNumbersGenerator');
  return (primeNumbersGenerator.isKnownPrimeNumber(num1) && primeNumbersGenerator.isKnownPrimeNumber(num2))
};

exports.solve = function (max, deep) {
  var primeNumbersGenerator = require('./math/primeNumbersGenerator');
  // warm up cache
  primeNumbersGenerator.getPrimeNumbers(1, max * max);

  var chains = [];
  var primeNumbers = primeNumbersGenerator.getPrimeNumbers(1, max);

  for (var i = 0, primeNumbersLen = primeNumbers.length; i < primeNumbersLen; i++) {
    var primeNumber = primeNumbers[i];
    for (var j = 0, chainsLen = chains.length; j < chainsLen; j++) {
      var chain = chains[j];
      var newChain = chain.concat([primeNumber]);
      if (exports.isMagicSet(newChain)) {
        chains.push(newChain);
        if (newChain.length >= deep) {
          console.log(newChain);
          return _.reduce(newChain, function(total, n) {
            total += n;
            return total;
          }, 0);
        }
      }
    }
    chains.push([primeNumber]);
  }
  return 0;
};