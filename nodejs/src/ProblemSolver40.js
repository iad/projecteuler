'use strict';

var exports = module.exports = {};

exports.generateNumber = function (digits) {
  var number = "";
  var i = 1;
  while (number.length < digits) {
    number += i.toString();
    i++;
  }
  return number;
};

exports.getDigit = function (number, position) {
  return parseInt(number.substr(position-1, 1));
};

exports.solve = function (maxD) {
  var number = exports.generateNumber(1000000);
  var getDigit = exports.getDigit;
  return getDigit(number, 1)
    * getDigit(number, 10)
    * getDigit(number, 100)
    * getDigit(number, 1000)
    * getDigit(number, 10000)
    * getDigit(number, 100000)
    * getDigit(number, 1000000);
};
