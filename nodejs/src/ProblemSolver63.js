'use strict';

var exports = module.exports = {};

exports.isCorrectNumber = function(n, p) {
  return Math.pow(n, p).toString().length == p;
};

exports.solve = function () {

  var count = 0;

  for (var i=1; i<=9; i++) {
    var pow = 1;
    while (true) {
      if (exports.isCorrectNumber(i, pow)) {
        count++;
      } else {
        break;
      }
      pow++
    }
  }

  return count;
};