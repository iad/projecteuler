'use strict';

var exports = module.exports = {};
var _ = require('lodash');
var BigInteger = require('big-integer');

exports.solve = function (target) {
  var ways = {};
  ways[0] = new BigInteger(1);

  for (var i of _.range(1, target)) {
    for (var j of _.range(i, target+1)) {
      ways[j] = (ways[j] == undefined) ? new BigInteger(0) : ways[j];
      ways[j] = ways[j].plus(ways[j - i])
    }
  }
  return ways[target].toString();
};
