'use strict';

var exports = module.exports = {};
var _ = require('lodash');
var lettersPermutationsGenerator = require('./str/lettersPermutationsGenerator');
var cubeNumbersGenerator = require('./math/cubeNumbersGenerator');

exports.solve = function (targetCount) {

  var i = 1;
  var sortedCubeNumbers = [];
  var smallestNumberForSortedCubeNumbers = [];

  while (true) {

    var cubeNumber = cubeNumbersGenerator.getCubeNumber(i);
    var sortedCubeNumber = cubeNumber.toString().split("").sort().join();

    if (sortedCubeNumbers[sortedCubeNumber] == undefined) {
      sortedCubeNumbers[sortedCubeNumber] = 0;
    }
    sortedCubeNumbers[sortedCubeNumber]++;

    if (smallestNumberForSortedCubeNumbers[sortedCubeNumber] == undefined) {
      smallestNumberForSortedCubeNumbers[sortedCubeNumber] = cubeNumber
    } else {
      if (smallestNumberForSortedCubeNumbers[sortedCubeNumber] > cubeNumber) {
        smallestNumberForSortedCubeNumbers[sortedCubeNumber] = cubeNumber;
      }
    }

    if (sortedCubeNumbers[sortedCubeNumber] == targetCount) {
      return smallestNumberForSortedCubeNumbers[sortedCubeNumber];
    }

    i++;
  }
};