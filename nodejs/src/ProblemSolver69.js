'use strict';

var exports = module.exports = {};
var _ = require('lodash');
var primeNumbersGenerator = require('./math/primeNumbersGenerator');

exports.solve = function (max) {
  var primeNumbers = primeNumbersGenerator.getPrimeNumbers(1, 1000);

  var result = 1;
  for (var primeNumber of primeNumbers) {
    if (result * primeNumber < max) {
      result *= primeNumber;
    } else {
      return result;
    }
  }
};
