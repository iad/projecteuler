'use strict';

var exports = module.exports = {};
var _ = require('lodash');
var BigNumber = require('bignumber.js');
var primeFactorsCalculator = require('./math/primeFactorsCalculator');

exports.solve = function(max) {
  var count = 0;
  var OneThird = (new BigNumber(1)).div(3);
  var OneHalf = (new BigNumber(1)).div(2);

  for (var i of _.range(1, max + 1)) {
    for (var j of _.range(i+1, max+1)) {
      if (primeFactorsCalculator.getBothFactors(i, j).length == 0) {
        var fraction = (new BigNumber(i)).div(j);

        if (fraction.lte(OneThird)) {
          break;
        }

        if (fraction.gte(OneHalf)) {
          continue;
        }

        count++;
      }
    }
  }

  return count;
};

