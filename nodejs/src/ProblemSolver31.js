'use strict';

var exports = module.exports = {};
var async = require('async');

exports.getSum = function (data, callback) {
  var sum = 0;

  async.forEachOf(data, function (value, key, callback) {
    sum = sum + value * key;
    callback();
  }, function (err) {
    if (err) {
      callback(new Error('getSum failed'))
    } else {
      callback(null, sum);
    }
  });
};

exports.solve = function (targetCount, callback) {
  var combinationsCount = 1;
  var coins = {
    1: 0,
    2: 0,
    5: 0,
    10: 0,
    20: 0,
    50: 0,
    100: 0,
    200: 0
  };
  var working = true;
  async.whilst(function () {
    return working;
  }, function (next) {
    async.setImmediate( function() {
      coins[1] = 0;

      coins[2]++;

      if (coins[2] * 2 > targetCount) {
        coins[5]++;
        coins[2] = 0;
      }
      if (coins[5] * 5 > targetCount) {
        coins[10]++;
        coins[5] = 0;
      }
      if (coins[10] * 10 > targetCount) {
        coins[20]++;
        coins[10] = 0;
      }
      if (coins[20] * 20 > targetCount) {
        coins[50]++;
        coins[20] = 0;
      }
      if (coins[50] * 50 > targetCount) {
        coins[100]++;
        coins[50] = 0;
      }
      if (coins[100] * 100 > targetCount) {
        coins[200]++;
        coins[100] = 0;
      }
      if (coins[200] * 200 > targetCount) {
        working = false;
      }

      exports.getSum(coins, function (err, currentSum) {
        coins[1] = targetCount - currentSum;
        if (coins[1] >= 0) {
          combinationsCount++;
        }
        next();
      })
    }, 0);
  }, function (err) {
    callback(null, combinationsCount);
  })
};
