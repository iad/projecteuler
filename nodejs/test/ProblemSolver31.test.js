var expect   = require("chai").expect;
describe("ProblemSolver31", function(){
  describe("MethodName", function() {

    var solver = require('../src/ProblemSolver31');

    it("Test getSum", function () {
      solver.getSum({1: 10, 2: 5}, function(err, sum) {
        expect(sum).equal(20);
      });
    });

    it("Test Solve", function (done) {
      this.timeout(5000000);
      solver.solve(200, function(err, count) {
        console.log(count);
        done();
      });
    });

  });
});

