var expect = require("chai").expect;
var sinon = require("sinon");

describe("ProblemSolver66", function () {
  //
  //describe("getYForXAndD", function () {
  //  var solver = require('../src/ProblemSolver66');
  //
  //  it("Test for 9 and 5", function () {
  //    var result = solver.getYForXAndD(9, 5);
  //    expect(result).equal('4');
  //  });
  //
  //  it("Test for 5 and 6", function () {
  //    var result = solver.getYForXAndD(5, 6);
  //    expect(result).equal('2');
  //  });
  //});

  describe("findX", function () {
    var solver = require('../src/ProblemSolver66');

    it("Test for 5", function () {
      var result = solver.findX(5);
      expect(result.toString()).equal("9");
    });

    it("Test for 7", function () {
      var result = solver.findX(7);
      expect(result.toString()).equal("8");
    });
  });

  describe("isSquareNumber", function () {
    var solver = require('../src/ProblemSolver66');

    it("Test for 8", function () {
      var result = solver.isSquareNumber(8);
      expect(result).equal(false);
    });

    it("Test for 9", function () {
      var result = solver.isSquareNumber(9);
      expect(result).equal(true);
    });
  });

  describe("solve", function () {
    var solver = require('../src/ProblemSolver66');

    it("Solve", function () {
      this.timeout(5000000);
      var result = solver.solve(1000);
      console.log(result);
    });
  });

});
