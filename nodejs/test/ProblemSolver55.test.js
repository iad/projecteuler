var expect = require("chai").expect;
var sinon = require("sinon");

describe("ProblemSolver55", function () {
  describe("isPalindromeNumber", function () {
    var solver = require('../src/ProblemSolver55');

    it("Test 7337", function () {
      var result = solver.isPalindromeNumber(7337);
      expect(result).equal(true);
    });

    it("Test 7333", function () {
      var result = solver.isPalindromeNumber(7333);
      expect(result).equal(false);
    });
  });

  describe("isLychrelNumber", function () {
    var solver = require('../src/ProblemSolver55');

    it("Test 349", function() {
      var result = solver.isLychrelNumber(349);
      expect(result).equal(false);
    });

    it("Test 10677", function() {
      var result = solver.isLychrelNumber(10677);
      expect(result).equal(true);
    });

    it("Test 4994", function() {
      var result = solver.isLychrelNumber(4994);
      expect(result).equal(true);
    });
  });

  describe("getReverseNumber", function () {
    var solver = require('../src/ProblemSolver55');

    it("Test 123", function () {
      var result = solver.getReverseNumber(123);
      expect(result).equal("321");
    });
  });

  describe("solve", function () {
    var solver = require('../src/ProblemSolver55');

    it("Test solve", function () {
      var result = solver.solve();
      console.log(result);
    });
  });

});

