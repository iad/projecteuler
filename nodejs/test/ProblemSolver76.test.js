var expect = require("chai").expect;
var sinon = require("sinon");

describe("ProblemSolver76", function () {
  describe("solve", function () {
    var solver = require('../src/ProblemSolver76');

    it("solve for 5", function () {
      var result = solver.solve(5);
      expect(result).is.eql("6");
    });

    it("solve for 6", function () {
      var result = solver.solve(6);
      expect(result).is.eql("10");
    });

    it("solve", function () {
      this.timeout(5000000);
      var result = solver.solve(100);
      console.log("result", result);
    });
  });
});


