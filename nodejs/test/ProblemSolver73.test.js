var expect = require("chai").expect;
var sinon = require("sinon");

describe("ProblemSolver73", function () {
  describe("solve", function () {
    var solver = require('../src/ProblemSolver73');

    it("solve for 8", function () {
      var result = solver.solve(8);
      expect(result).is.eql(3);
    });

    it("solve for 12000", function () {
      this.timeout(5000000);
      var result = solver.solve(12000);
      console.log("result=", result);
    });
  });
});


