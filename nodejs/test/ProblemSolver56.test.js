var expect = require("chai").expect;
var sinon = require("sinon");

describe("ProblemSolver56", function () {
  describe("getPow", function () {
    var solver = require('../src/ProblemSolver56');

    it("Test 10^6", function () {
      var result = solver.getPow(10, 6);
      expect(result).equal("1000000");
    });
  });

  describe("getSumOfDigits", function () {
    var solver = require('../src/ProblemSolver56');

    it("Test 1234567890", function () {
      var result = solver.getSumOfDigits(1234567890);
      expect(result).equal(45);
    });
  });

  describe("solve", function () {
    var solver = require('../src/ProblemSolver56');

    it("Test solve", function () {
      var result = solver.solve(100);
      console.log(result);
    });
  });

});

