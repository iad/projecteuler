var expect   = require("chai").expect;
var sinon = require("sinon");

describe("ProblemSolver52", function(){
  describe("compareLettersCount", function() {
    var solver = require('../src/ProblemSolver52');

    it("Test with short numbers", function () {
      var result = solver.compareLettersCount(123, 321);
      expect(result).equal(true);
    });

    it("Test with numbers with different digits", function () {
      var result = solver.compareLettersCount(123, 432);
      expect(result).equal(false);
    });

    it("Test with long numbers", function () {
      var result = solver.compareLettersCount(12312344444, 33444414212);
      expect(result).equal(true);
    });
  });

  describe("checkNumber", function() {
    var solver = require('../src/ProblemSolver52');

    it("Test that compareLettersCount called", function () {
      var spy = sinon.spy(solver, "compareLettersCount");
      var result = solver.checkNumber(125874);
      expect(spy.called).equal(true);
    });
  });

  describe("solve", function() {
    var solver = require('../src/ProblemSolver52');

    it("solution", function (done) {
      this.timeout(5000000);
      solver.solve(function(err, result) {
        console.log(result);
        done();
      });
    });
  });

});

