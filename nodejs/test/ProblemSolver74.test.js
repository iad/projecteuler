var expect = require("chai").expect;
var sinon = require("sinon");

describe("ProblemSolver74", function () {
  describe("getSumFactorialsOfDigits", function () {
    var solver = require('../src/ProblemSolver74');

    it("test for 169", function () {
      var result = solver.getSumFactorialsOfDigits(169);
      expect(result).is.eql(363601);
    });

    it("test for 363601", function () {
      var result = solver.getSumFactorialsOfDigits(363601);
      expect(result).is.eql(1454);
    });
  });

  describe("getFactorial", function () {
    var solver = require('../src/ProblemSolver74');

    it("test for 5", function () {
      var result = solver.getFactorial(5);
      expect(result).is.eql(120);
    });
  });

  describe("getDigitFactorialChain", function () {
    var solver = require('../src/ProblemSolver74');

    it("test for 145", function () {
      var result = solver.getDigitFactorialChain(145);
      expect(result).is.eql([145]);
    });

    it("test for 169", function () {
      var result = solver.getDigitFactorialChain(169);
      expect(result).is.eql([169, 363601, 1454]);
    });

    it("test for 69", function () {
      var result = solver.getDigitFactorialChain(69);
      expect(result).is.eql([69, 363600, 1454, 169, 363601]);
    });

    it("test for 4971", function () {
      var result = solver.getDigitFactorialChain(4971);
      expect(result.length).is.eql(60);
    });

  });

  describe("solve", function () {
    var solver = require('../src/ProblemSolver74');

    it("solve", function () {
      this.timeout(5000000);
      var result = solver.solve(1000000);
      console.log("result", result);
    });
  });

});


