var expect = require("chai").expect;
describe("ProblemSolver2", function () {
  describe("solve", function () {

    var solver = require('../src/ProblemSolver2');

    it("Test solve for 10", function () {
      var result = solver.solve(10);
      expect(result).equal('10');
    });

    it("Test Solve", function () {
      this.timeout(5000000);
      var result = solver.solve(4000000);
      console.log(result);
    });
  });
});

