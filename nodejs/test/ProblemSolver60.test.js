var expect = require("chai").expect;
var sinon = require("sinon");

describe("ProblemSolver60", function () {

  describe("getCombinations", function () {
    var solver = require('../src/ProblemSolver60');

    it("Test getCombinations for deep 2", function () {
      var getCombinations = solver.getCombinations([3, 7, 11, 100], 2);
      var result = [];
      var set = [];
      while (set = getCombinations.next().value) {
        result.push(set);
      }
      expect(result).is.eql([
        [7, 3],
        [11, 3],
        [11, 7],
        [100, 3],
        [100, 7],
        [100, 11]
      ]);
    });

    it("Test getCombinations for deep 3", function () {
      var getCombinations = solver.getCombinations([3, 7, 109, 11], 3);
      var result = [];
      var set = [];
      while (set = getCombinations.next().value) {
        result.push(set);
      }
      expect(result).is.eql([
        [109, 7, 3], [109, 11, 3], [109, 11, 7], [11, 7, 3]
      ]);
    });
  });

  describe("isMagicSet", function () {
    var solver = require('../src/ProblemSolver60');

    it("Test isMagicSet for valid prime numbers", function () {
      var result = solver.isMagicSet([3, 7, 109, 673]);
      expect(result).equal(true);
    });

    it("Test isMagicSet for not valid prime numbers", function () {
      var result = solver.isMagicSet([3, 7, 109, 674]);
      expect(result).equal(false);
    });
  });

  describe("solve", function () {
    var solver = require('../src/ProblemSolver60');

    it("Test for 4 of 1000 numbers", function () {
      this.timeout(5000000);
      var result = solver.solve(1000, 4);
      expect(result).is.eql(792);
    });

    xit("Test for 5 of 10000 numbers", function () {
      this.timeout(5000000);
      var result = solver.solve(10000, 5);
      console.log(result);
      // [ 13, 5197, 5701, 6733, 8389 ]
      // 26033
      // 472409ms
    });
  });


});

