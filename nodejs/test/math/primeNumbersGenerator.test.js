var expect = require("chai").expect;
var sinon = require("sinon");

describe("primeNumbersGenerator", function () {
  describe("isKnownPrimeNumber", function () {
    var solver = require('../../src/math/primeNumbersGenerator');

    it("Test that 19 is a prime number", function () {
      var result = solver.isKnownPrimeNumber(19);
      expect(result).equal(true);
    });

    it("Test that 18 is not a prime number", function () {
      var result = solver.isKnownPrimeNumber(18);
      expect(result).equal(false);
    });

  });

  describe("getPrimeNumbers", function () {
    var solver = require('../../src/math/primeNumbersGenerator');

    it("Test for 10", function () {
      var result = solver.getPrimeNumbers(1, 10);
      expect(result).to.eql([2, 3, 5, 7]);
    });

    it("Test from 10 to 20", function () {
      var result = solver.getPrimeNumbers(10, 20);
      expect(result).to.eql([11, 13, 17, 19]);
    });
  });


  describe("isKnownPrimeNumber", function () {
    var solver = require('../../src/math/primeNumbersGenerator');

    it("Test from 10 to 23", function () {
      var result = solver.isKnownPrimeNumber(23);
      expect(result).equal(true);
    });

    it("Test for 24", function () {
      var result = solver.isKnownPrimeNumber(24);
      expect(result).equal(false);
    });

    it("Test for 32", function () {
      var result = solver.isKnownPrimeNumber(32);
      expect(result).equal(false);
    });
  });



});

