var expect = require("chai").expect;
var sinon = require("sinon");

describe("cubeNumbersGenerator", function () {
  describe("getCubeNumber", function () {
    var solver = require('../../src/math/cubeNumbersGenerator');

    it("Test for 3", function () {
      var result = solver.getCubeNumber(3);
      expect(result).equal(27);
    });

    it("Test for 405", function () {
      var result = solver.getCubeNumber(405);
      expect(result).equal(66430125);
    });
  });

  describe("isKnownCubeNumber", function () {
    var solver = require('../../src/math/cubeNumbersGenerator');

    it("Test for 27", function () {
      var result = solver.isKnownCubeNumber(27);
      expect(result).equal(true);
    });

    it("Test for 66430125", function () {
      var result = solver.isKnownCubeNumber(66430125);
      expect(result).equal(true);
    });

    it("Test for not cube number 66430126", function () {
      var result = solver.isKnownCubeNumber(66430126);
      expect(result).equal(false);
    });
  });

  describe("getCubeNumbers", function () {
    var solver = require('../../src/math/cubeNumbersGenerator');

    it("Test for 100 to 1000", function () {
      var result = solver.getCubeNumbers(100, 1000);
      expect(result).to.eql([125, 216, 343, 512, 729, 1000]);
    });
  });
});

