'use strict';

var expect = require("chai").expect;
var sinon = require("sinon");

describe("primeFactorsCalculator", function () {
  describe("getFactors", function () {
    var solver = require('../../src/math/primeFactorsCalculator');

    it("Test for 8", function () {
      var result = solver.getFactors(8);
      expect(result).is.eql([2]);
    });

    it("Test for 28", function () {
      var result = solver.getFactors(28);
      expect(result).is.eql([2, 7]);
    });
  });

  describe("getBothFactors", function () {
    var solver = require('../../src/math/primeFactorsCalculator');

    it("Test for 10, 28", function () {
      var result = solver.getBothFactors(10, 28);
      expect(result).is.eql([2]);
    });

    it("Test for 6, 12", function () {
      var result = solver.getBothFactors(6, 12);
      expect(result).is.eql([2, 3]);
    });

    it("Test for 3, 7", function () {
      var result = solver.getBothFactors(3, 7);
      expect(result).is.eql([]);
    });
  });
});