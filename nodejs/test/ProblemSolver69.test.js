var expect = require("chai").expect;
var sinon = require("sinon");

describe("ProblemSolver67", function () {
  describe("solve", function () {
    var solver = require('../src/ProblemSolver67');

    it("solve for 10", function () {
      var result = solver.solve(10);
      expect(result).equal(6);
    });

    it("Solve", function () {
      this.timeout(5000000);
      var result = solver.solve(1000000);
      console.log(result);
    });
  });

});
