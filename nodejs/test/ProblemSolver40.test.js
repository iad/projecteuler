var expect = require("chai").expect;
var sinon = require("sinon");

describe("ProblemSolver40", function () {
   describe("generateNumber", function () {
    var solver = require('../src/ProblemSolver40');

    it("Test for 12", function () {
      var result = solver.generateNumber(12);
      expect(result).equal("1234567891011");
    });
  });

  describe("getDigit", function () {
    var solver = require('../src/ProblemSolver40');

    it("Test for 1234567891011 and position 12", function () {
      var result = solver.getDigit("1234567891011", 12);
      expect(result).equal(1);
    });

    it("Test for 1234567891011 and position 5", function () {
      var result = solver.getDigit("1234567891011", 5);
      expect(result).equal(5);
    });

  });

  describe("solve", function () {
    var solver = require('../src/ProblemSolver40');

    it("solve", function () {
      var result = solver.solve();
      console.log("result = ", result)
    });
  });
});
