var expect = require("chai").expect;
var sinon = require("sinon");


describe("lettersPermutationsGenerator", function () {
  describe("getPermutations", function () {
    var lettersPermutationsGenerator = require('../../src/str/lettersPermutationsGenerator');

    it("Test for abc", function () {
      var result = lettersPermutationsGenerator.getPermutations("abc");
      expect(result).to.eql(["abc", "acb", "bac", "bca", "cab", "cba"]);
    });
  });
});

