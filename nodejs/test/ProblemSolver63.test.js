var expect = require("chai").expect;
var sinon = require("sinon");

describe("ProblemSolver63", function () {

  describe("isCorrectNumber", function () {
    var solver = require('../src/ProblemSolver63');

    it("Test for 7^5", function () {
      var result = solver.isCorrectNumber(7, 5);
      expect(result).equal(true);
    });

    it("Test for 8^9", function () {
      var result = solver.isCorrectNumber(8, 9);
      expect(result).equal(true);
    });

    it("Test for invalid params 11^4", function () {
      var result = solver.isCorrectNumber(11, 4);
      expect(result).equal(false);
    });
  });


  describe("solve", function () {
    var solver = require('../src/ProblemSolver63');

    it("Solve", function () {
      var result = solver.solve();
      console.log(result);
    });
  });

});

