var expect = require("chai").expect;
var sinon = require("sinon");

describe("ProblemSolver71", function () {
  describe("solve", function () {
    var solver = require('../src/ProblemSolver71');

    it("solve for 8", function () {
      var result = solver.solve(8);
      expect(result).equal('2');
    });

    it("Solve", function () {
      this.timeout(5000000);
      var result = solver.solve(1000000);
      console.log(result);
    });
  });

});
