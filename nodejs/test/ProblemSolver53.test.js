var expect = require("chai").expect;
var sinon = require("sinon");

describe("ProblemSolver53", function () {
  describe("getCountOfSelections", function () {
    var solver = require('../src/ProblemSolver53');

    it("Test 3 from 5", function () {
      var result = solver.getCountOfSelections(5, 3);
      expect(result.toString()).equal("10");
    });

    it("Test 10 from 23", function () {
      var result = solver.getCountOfSelections(23, 10);
      expect(result.toString()).equal("1144066");
    });

  });

  describe("solve", function () {
    var solver = require('../src/ProblemSolver53');

    it("Test solve", function () {
      var result = solver.solve(5, 0);
      expect(result).equal(10);
    });

    it("solve", function () {
      this.timeout(5000000);
      var result = solver.solve(100, 1000000);
      console.log(result);
    });
  });
});

