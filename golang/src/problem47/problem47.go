package main

import (
	"../mymath"
	"fmt"
	"time"
)

func checkNumber(num int64, primeNumbers []int64) bool {
	if mymath.GetCountOfPrimeDivisors(num, primeNumbers) == 4 {
		return true
	}
	return false
}

func check4Numbers(num int64, primeNumbers []int64) bool {
	if checkNumber(num, primeNumbers) && checkNumber(num+1, primeNumbers) && checkNumber(num+2, primeNumbers) && checkNumber(num+3, primeNumbers) {
		return true
	}
	return false
}

func main() {
	t0 := time.Now()
	i := 1

	primeNumbers := mymath.GetPrimeNumbers(0, 10000)
	for {
		if check4Numbers(int64(i), primeNumbers) {
			fmt.Println(i)
			t1 := time.Now()
			fmt.Printf("The call took %v to run.\n", t1.Sub(t0))
			return
		} else {
			i++
		}
	}
}
