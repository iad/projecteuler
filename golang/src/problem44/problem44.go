package main

import (
	"fmt"
)

var pentagonNumbers = make(map[int64]int64)
var knownPentagonNumbers = make(map[int64]bool)
var minimalDifference = int64(100000000)

func getPentagonNumber(n int64) (value int64) {
	if v, ok := pentagonNumbers[n]; ok {
		value = v
	} else {
		value = n * (3*n - 1) / 2
		pentagonNumbers[n] = value
		knownPentagonNumbers[value] = true
	}
	return
}

func isPentagonNumber(value int64) bool {
	if _, ok := knownPentagonNumbers[value]; ok {
		return true
	}
	return false
}

func checkNumbers(num1, num2 int64) bool {
	if sumNum := num1 + num2; isPentagonNumber(sumNum) {
		if resultNum := num1 - num2; isPentagonNumber(resultNum) {
			if resultNum < minimalDifference {
				minimalDifference = resultNum
				//				fmt.Println(minimalDifference)
			}
		}
	}
	return true
}

func upPentagonNumbers() {
	for i := 1; i < 1000000; i++ {
		getPentagonNumber(int64(i))
	}
}

func main() {
	upPentagonNumbers()
	for i := 1; i < 1000000; i++ {
		fmt.Println(i, minimalDifference)
		for j := i - 1; j > 0; j-- {
			go checkNumbers(getPentagonNumber(int64(i)), getPentagonNumber(int64(j)))
		}
	}
}
