package main

import (
	"reflect"
	"testing"
)

func Test_getExcludedValuesFromCluster(t *testing.T) {
	type args struct {
		task    Task
		targetI int
		targetJ int
	}
	tests := []struct {
		name string
		args args
		want map[int64]struct{}
	}{
		struct {
			name string
			args args
			want map[int64]struct{}
		}{
			name: "",
			args: args{
				task: Task{
					Name: "",
					Rows: []TaskRow{
						{0, 0, 3, 0, 2, 0, 6, 0, 0},
						{9, 0, 0, 3, 0, 5, 0, 0, 1},
						{0, 0, 1, 8, 0, 6, 4, 0, 0},
						{0, 0, 8, 1, 0, 2, 9, 0, 0},
						{7, 0, 0, 0, 0, 0, 0, 0, 8},
						{0, 0, 6, 7, 0, 8, 2, 0, 0},
						{0, 0, 2, 6, 0, 9, 5, 0, 0},
						{8, 0, 0, 2, 0, 3, 0, 0, 9},
						{0, 0, 5, 0, 1, 0, 3, 0, 0},
					},
				},
				targetI: 0,
				targetJ: 0,
			},
			want: map[int64]struct{}{
				3: struct{}{},
				9: struct{}{},
				1: struct{}{},
			},
		},
		{
			name: "",
			args: args{
				task: Task{
					Name: "",
					Rows: []TaskRow{
						{0, 0, 3, 0, 2, 0, 6, 0, 0},
						{9, 0, 0, 3, 0, 5, 0, 0, 1},
						{0, 0, 1, 8, 0, 6, 4, 0, 0},
						{0, 0, 8, 1, 0, 2, 9, 0, 0},
						{7, 0, 0, 0, 0, 0, 0, 0, 8},
						{0, 0, 6, 7, 0, 8, 2, 0, 0},
						{0, 0, 2, 6, 0, 9, 5, 0, 0},
						{8, 0, 0, 2, 0, 3, 0, 0, 9},
						{0, 0, 5, 0, 1, 0, 3, 0, 0},
					},
				},
				targetI: 2,
				targetJ: 2,
			},
			want: map[int64]struct{}{
				3: struct{}{},
				9: struct{}{},
				1: struct{}{},
			},
		},
		{
			name: "",
			args: args{
				task: Task{
					Name: "",
					Rows: []TaskRow{
						{0, 0, 3, 0, 2, 0, 6, 0, 0},
						{9, 0, 0, 3, 0, 5, 0, 0, 1},
						{0, 0, 1, 8, 0, 6, 4, 0, 0},
						{0, 0, 8, 1, 0, 2, 9, 0, 0},
						{7, 0, 0, 0, 0, 0, 0, 0, 8},
						{0, 0, 6, 7, 0, 8, 2, 0, 0},
						{0, 0, 2, 6, 0, 9, 5, 0, 0},
						{8, 0, 0, 2, 0, 3, 0, 0, 9},
						{0, 0, 5, 0, 1, 0, 3, 0, 0},
					},
				},
				targetI: 8,
				targetJ: 8,
			},
			want: map[int64]struct{}{
				3: struct{}{},
				5: struct{}{},
				9: struct{}{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getExcludedValuesFromCluster(tt.args.task, tt.args.targetI, tt.args.targetJ); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getExcludedValuesFromCluster() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getPossibleValues(t *testing.T) {
	type args struct {
		task    Task
		targetI int
		targetJ int
	}
	tests := []struct {
		name string
		args args
		want []int64
	}{
		struct {
			name string
			args args
			want []int64
		}{
			name: "",
			args: struct {
				task    Task
				targetI int
				targetJ int
			}{
				task: Task{
					Name: "",
					Rows: []TaskRow{
						{0, 0, 3, 0, 2, 0, 6, 0, 0},
						{9, 0, 0, 3, 0, 5, 0, 0, 1},
						{0, 0, 1, 8, 0, 6, 4, 0, 0},
						{0, 0, 8, 1, 0, 2, 9, 0, 0},
						{7, 0, 0, 0, 0, 0, 0, 0, 8},
						{0, 0, 6, 7, 0, 8, 2, 0, 0},
						{0, 0, 2, 6, 0, 9, 5, 0, 0},
						{8, 0, 0, 2, 0, 3, 0, 0, 9},
						{0, 0, 5, 0, 1, 0, 3, 0, 0},
					},
				},
				targetI: 0,
				targetJ: 0,
			},
			want: []int64{4, 5},
		},
		{
			name: "",
			args: struct {
				task    Task
				targetI int
				targetJ int
			}{
				task: Task{
					Name: "",
					Rows: []TaskRow{
						{0, 0, 3, 0, 2, 0, 6, 0, 0},
						{9, 0, 0, 3, 0, 5, 0, 0, 1},
						{0, 0, 1, 8, 0, 6, 4, 0, 0},
						{0, 0, 8, 1, 0, 2, 9, 0, 0},
						{7, 0, 0, 0, 0, 0, 0, 0, 8},
						{0, 0, 6, 7, 0, 8, 2, 0, 0},
						{0, 0, 2, 6, 0, 9, 5, 0, 0},
						{8, 0, 0, 2, 0, 3, 0, 0, 9},
						{0, 0, 5, 0, 1, 0, 3, 0, 0},
					},
				},
				targetI: 8,
				targetJ: 8,
			},
			want: []int64{2, 4, 6, 7},
		},
		{
			name: "",
			args: struct {
				task    Task
				targetI int
				targetJ int
			}{
				task: Task{
					Name: "",
					Rows: []TaskRow{
						{0, 0, 3, 0, 2, 0, 6, 0, 0},
						{9, 0, 0, 3, 0, 5, 0, 0, 1},
						{0, 0, 1, 8, 0, 6, 4, 0, 0},
						{0, 0, 8, 1, 0, 2, 9, 0, 0},
						{7, 0, 0, 0, 0, 0, 0, 0, 8},
						{0, 0, 6, 7, 0, 8, 2, 0, 0},
						{0, 0, 2, 6, 0, 9, 5, 0, 0},
						{8, 0, 0, 2, 0, 3, 0, 0, 9},
						{0, 0, 5, 0, 1, 0, 3, 0, 0},
					},
				},
				targetI: 0,
				targetJ: 3,
			},
			want: []int64{4, 9},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getPossibleValues(tt.args.task, tt.args.targetI, tt.args.targetJ); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getPossibleValues() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_solveTask(t *testing.T) {
	type args struct {
		task Task
	}
	tests := []struct {
		name    string
		args    args
		want    Task
		wantErr bool
	}{
		struct {
			name    string
			args    args
			want    Task
			wantErr bool
		}{
			name: "",
			args: args{
				struct {
					Name string
					Rows []TaskRow
				}{
					Name: "",
					Rows: []TaskRow{
						{0, 0, 3, 0, 2, 0, 6, 0, 0},
						{9, 0, 0, 3, 0, 5, 0, 0, 1},
						{0, 0, 1, 8, 0, 6, 4, 0, 0},
						{0, 0, 8, 1, 0, 2, 9, 0, 0},
						{7, 0, 0, 0, 0, 0, 0, 0, 8},
						{0, 0, 6, 7, 0, 8, 2, 0, 0},
						{0, 0, 2, 6, 0, 9, 5, 0, 0},
						{8, 0, 0, 2, 0, 3, 0, 0, 9},
						{0, 0, 5, 0, 1, 0, 3, 0, 0},
					},
				},
			},
			want: Task{
				Name: "",
				Rows: []TaskRow{
					{4, 8, 3, 9, 2, 1, 6, 5, 7},
					{9, 6, 7, 3, 4, 5, 8, 2, 1},
					{2, 5, 1, 8, 7, 6, 4, 9, 3},
					{5, 4, 8, 1, 3, 2, 9, 7, 6},
					{7, 2, 9, 5, 6, 4, 1, 3, 8},
					{1, 3, 6, 7, 9, 8, 2, 4, 5},
					{3, 7, 2, 6, 8, 9, 5, 1, 4},
					{8, 1, 4, 2, 5, 3, 7, 6, 9},
					{6, 9, 5, 4, 1, 7, 3, 8, 2},
				},
			},
			wantErr: false,
		},
		{
			name: "",
			args: args{
				struct {
					Name string
					Rows []TaskRow
				}{
					Name: "",
					Rows: []TaskRow{
						{0, 0, 0, 0, 0, 3, 0, 1, 7},
						{0, 1, 5, 0, 0, 9, 0, 0, 8},
						{0, 6, 0, 0, 0, 0, 0, 0, 0},
						{1, 0, 0, 0, 0, 7, 0, 0, 0},
						{0, 0, 9, 0, 0, 0, 2, 0, 0},
						{0, 0, 0, 5, 0, 0, 0, 0, 4},
						{0, 0, 0, 0, 0, 0, 0, 2, 0},
						{5, 0, 0, 6, 0, 0, 3, 4, 0},
						{3, 4, 0, 2, 0, 0, 0, 0, 0},
					},
				},
			},
			want: Task{
				Name: "",
				Rows: []TaskRow{
					{2, 9, 4, 8, 6, 3, 5, 1, 7},
					{7, 1, 5, 4, 2, 9, 6, 3, 8},
					{8, 6, 3, 7, 5, 1, 4, 9, 2},
					{1, 5, 2, 9, 4, 7, 8, 6, 3},
					{4, 7, 9, 3, 8, 6, 2, 5, 1},
					{6, 3, 8, 5, 1, 2, 9, 7, 4},
					{9, 8, 6, 1, 3, 4, 7, 2, 5},
					{5, 2, 1, 6, 7, 8, 3, 4, 9},
					{3, 4, 7, 2, 9, 5, 1, 8, 6},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := solveTask(tt.args.task, 0)
			if (err != nil) != tt.wantErr {
				t.Errorf("solveTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("solveTask() got = %v, want %v", got, tt.want)
			}
		})
	}
}
