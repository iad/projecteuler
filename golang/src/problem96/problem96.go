package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type Task struct {
	Name string
	Rows []TaskRow
}

func (t Task) Copy() Task {
	c := Task{
		Name: t.Name,
		Rows: make([]TaskRow, 0),
	}

	for _, row := range t.Rows {
		vs := make([]int64, 0)
		for _, item := range row {
			vs = append(vs, item)
		}
		c.Rows = append(c.Rows, vs)
	}

	return c
}

func (t Task) String() string {
	str := fmt.Sprintf("Task: %s\n", t.Name)
	for _, row := range t.Rows {
		str += fmt.Sprintf("   ")
		for _, value := range row {
			str += fmt.Sprintf("%v, ", value)
		}
		str += fmt.Sprintf("\n")
	}

	return str
}

type TaskRow []int64

func main() {
	fileData, err := readFile("p096_sudoku.txt")
	if err != nil {
		log.Fatal(err)
	}

	tasks, err := prepareTasks(fileData)
	if err != nil {
		log.Fatal(err)
	}

	solutions, err := solve(tasks)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(solutions)

	result := getSpecialValue(solutions)
	fmt.Println(result)
}

func getSpecialValue(solutions []Task) int64 {
	result := int64(0)

	for _, solution := range solutions {
		value := solution.Rows[0][0]*100 + solution.Rows[0][1]*10 + solution.Rows[0][2]
		//fmt.Println(value)
		result += value
	}

	return result
}

func solve(tasks []Task) ([]Task, error) {
	solutions := make([]Task, 0)
	for i, task := range tasks {
		fmt.Printf("Processing %v. ", i)
		solution, err := solveTask(task, 0)
		if err != nil {
			return nil, err
		}

		if !isValidSolution(solution) {
			return solutions, fmt.Errorf("Invalidid solution for %v", i)
		}

		fmt.Print("Done\n")
		solutions = append(solutions, solution)
	}

	return solutions, nil
}

func solveTask(task Task, level int64) (Task, error) {
	var foundNewValue bool

	if level > 2 {
		return task, fmt.Errorf("Too deep")
	}

	for {
		if isValidSolution(task) {
			return task, nil
		}

		foundNewValue = false
		variants := make(map[int]map[int]map[int64]struct{})

		for i, row := range task.Rows {
			for j, value := range row {
				if value != 0 {
					continue
				}

				possibleValues := getPossibleValues(task, i, j)
				if len(possibleValues) == 1 {
					foundNewValue = true
					//fmt.Printf("1. %s. For %v, %v. Simple :%v\n", task.Name, i, j, possibleValues[0])
					task.Rows[i][j] = possibleValues[0]
				}

				for _, value := range possibleValues {
					if _, exists := variants[i]; !exists {
						variants[i] = make(map[int]map[int64]struct{})
					}

					if _, exists := variants[i][j]; !exists {
						variants[i][j] = make(map[int64]struct{})
					}

					variants[i][j][value] = struct{}{}
				}
			}
		}

		if isValidSolution(task) {
			return task, nil
		}

		if foundNewValue {
			continue
		}

		// second step. select values excluded by neighbors
		for i, row := range task.Rows {
			for j, value := range row {
				if value != 0 {
					continue
				}

				possibleValues := getPossibleValues(task, i, j)
				lockedValues := getLockedValues(variants, i, j)
				//fmt.Printf("Locked for %v, %v : %+v. Possible :%+v\n", i, j, lockedValues, possibleValues)
				for _, possibleValue := range possibleValues {
					if _, locked := lockedValues[possibleValue]; !locked {
						//fmt.Printf("2. %s. For %v, %v. Deduction :%v\n", task.Name, i, j, possibleValue)
						foundNewValue = true
						task.Rows[i][j] = possibleValue
						continue
					}
				}

			}
		}

		if isValidSolution(task) {
			return task, nil
		}

		if foundNewValue {
			continue
		}

		for i, row := range task.Rows {
			for j, value := range row {
				if value != 0 {
					continue
				}

				possibleValues := getPossibleValues(task, i, j)
				if len(possibleValues) == 0 {
					//fmt.Printf("x\n")
					return task, fmt.Errorf("Empty tree")
				}

				//fmt.Println(task)

				for _, possibleValue := range possibleValues {
					assumption := task.Copy()
					assumption.Rows[i][j] = possibleValue
					//fmt.Printf("3. %s. For %v, %v. Assumption :%v\n", task.Name, i, j, possibleValue)
					//fmt.Printf("%v. %v %v: %v > ", level, i, j, possibleValue)
					solution, err := solveTask(assumption, level+1)
					if err != nil {
						//fmt.Println(err)
						continue
					}
					if isValidSolution(solution) {
						return solution, nil
					}
				}
			}
		}

		if foundNewValue {
			continue
		}

		// third step. recursive assumption

		break
	}

	return task, nil
}

func getLockedValues(variants map[int]map[int]map[int64]struct{}, targetI, targetJ int) map[int64]struct{} {
	result := make(map[int64]struct{})

	for i, row := range variants {
		for j, values := range row {
			if i == targetI && j == targetJ {
				continue
			}

			if (targetI/3) == (i/3) && (targetJ/3) == (j/3) {
				// ignore the same section
				continue
			}

			if i == targetI || j == targetJ {
				for value := range values {
					result[value] = struct{}{}
				}
			}
		}
	}

	return result
}

func isValidSolution(task Task) bool {
	for i, row := range task.Rows {
		for j, value := range row {
			if value == 0 {
				return false
			}

			possibleValues := getPossibleValues(task, i, j)
			if len(possibleValues) != 1 {
				return false
			}

			if possibleValues[0] != value {
				return false
			}
		}
	}

	return true
}

func getPossibleValues(task Task, targetI, targetJ int) []int64 {
	excludedValues := make(map[int64]struct{}, 0)
	for i, row := range task.Rows {
		for j, value := range row {
			if i == targetI && j == targetJ {
				continue
			}

			if value == 0 {
				continue
			}

			if i == targetI || j == targetJ {
				excludedValues[value] = struct{}{}
			}
		}
	}

	for excluded := range getExcludedValuesFromCluster(task, targetI, targetJ) {
		excludedValues[excluded] = struct{}{}
	}

	result := make([]int64, 0)

	for possibleValue := int64(1); possibleValue <= 9; possibleValue++ {
		if _, found := excludedValues[possibleValue]; !found {
			result = append(result, possibleValue)
		}
	}

	return result
}

func getExcludedValuesFromCluster(task Task, targetI, targetJ int) map[int64]struct{} {
	startI := (targetI / 3) * 3
	startJ := (targetJ / 3) * 3

	result := make(map[int64]struct{})

	for i := startI; i <= startI+2; i++ {
		for j := startJ; j <= startJ+2; j++ {
			if i == targetI && j == targetJ {
				continue
			}

			value := task.Rows[i][j]
			if value != 0 {
				result[value] = struct{}{}
			}
		}
	}

	return result
}

func prepareTasks(lines []string) ([]Task, error) {
	tasks := make([]Task, 0)
	task := Task{}
	for i, line := range lines {
		if strings.Contains(line, "Grid") {
			if i != 0 {
				if len(task.Rows) != 9 {
					return nil, fmt.Errorf("the task %s has %v rows, but expected 9. Line %v", task.Name, len(task.Rows), i)
				}

				tasks = append(tasks, task)
				task = Task{
					Name: line,
					Rows: make([]TaskRow, 0),
				}

			}
			continue
		}

		values, err := parseLineValues(line)
		if err != nil {
			return nil, fmt.Errorf("the task %s has a row with %v values, but expected 9. Err %s", task.Name, len(values), err)
		}
		task.Rows = append(task.Rows, values)
	}

	tasks = append(tasks, task)

	return tasks, nil
}

func parseLineValues(line string) (TaskRow, error) {
	values := make(TaskRow, 0)
	for _, valueStr := range line {
		value, err := strconv.Atoi(string(valueStr))
		if err != nil {
			return nil, err
		}

		values = append(values, int64(value))
	}

	if len(values) != 9 {
		return nil, fmt.Errorf("received %v values in row", len(values))
	}

	return values, nil
}

func readFile(fileName string) ([]string, error) {
	var lines []string

	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println(err)
		return lines, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, nil
}
