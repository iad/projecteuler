package main

import (
	"fmt"
	"strconv"
)

type Num int64

func (self Num) GetEndNum() int64 {
	if val, ok := n[int64(self)]; ok {
		return val
	}

	if self == 1 {
		return 1
	}

	if self == 89 {
		return 89
	}

	next := Num(getNextNumber(int64(self)))
	end := next.GetEndNum()
	n[int64(self)] = end

	return end
}

var n map[int64]int64 = make(map[int64]int64)

func main() {
	n81 := int64(0)

	for i := 1; i < 10000000; i++ {
		n := Num(i)
		end := n.GetEndNum()

		if end == 89 {
			n81++
		}
	}

	fmt.Println("n81", n81)
}

func getNextNumber(n int64) int64 {
	str := strconv.FormatInt(n, 10)
	res := int64(0)

	for i := 0; i < len(str); i++ {
		l := string(str[i])
		ln, _ := strconv.ParseInt(l, 10, 64)
		res += ln * ln
	}

	return res
}
