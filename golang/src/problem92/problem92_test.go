package main

import (
	"fmt"
	"testing"
	"time"
)

func TestGetNextNumber(t *testing.T) {
	if result := getNextNumber(44); result != 32 {
		t.Error("Expected 32, got ", result)
	}
}

func Test(t *testing.T) {
	t0 := time.Now()

	main()

	t1 := time.Now()
	fmt.Printf("The call took %v to run.\n", t1.Sub(t0))

	//260324
	//The call took 24.599538ms to run.
}
