package main

import (
	"fmt"
	"testing"
	"time"
)

func TestGetNumberOfWays(t *testing.T) {
	num := Num{
		Value:         10,
		PreviousPrime: 10,
	}

	if result := num.getNumberOfPrimeDivizorChains(); result != 5 {
		t.Error("Expected 5", result)
	}
}

func Test(t *testing.T) {
	t0 := time.Now()

	result := solve()
	fmt.Println(result)

	t1 := time.Now()
	fmt.Printf("The call took %v to run.\n", t1.Sub(t0))

	//71
	//The call took 2.367624ms to run.

}
