package main

import (
	"../mymath"
)

type Num struct {
	Value         int64
	PreviousPrime int64
}

func (n *Num) getNumberOfPrimeDivizorChains() int64 {

	cacheForNumber, ok := numberOfWays[n.Value]
	if ok {
		cacheForNumberAndPrimeNumber, ok2 := cacheForNumber[n.PreviousPrime]
		if ok2 {
			return cacheForNumberAndPrimeNumber
		}
	}

	primeNumbers := mymath.GetPrimeNumbers(1, n.PreviousPrime)
	var total = int64(0)

	for _, primeNumber := range primeNumbers {
		var diff = n.Value - primeNumber
		if diff == 0 {
			total++
			continue
		}
		if diff < 0 {
			continue
		}

		newNum := Num{
			Value:         n.Value - primeNumber,
			PreviousPrime: primeNumber,
		}
		total += newNum.getNumberOfPrimeDivizorChains()
	}

	cacheForNumber, ok3 := numberOfWays[n.Value]
	if !ok3 {
		numberOfWays[n.Value] = NumCache{}
	}

	numberOfWays[n.Value][n.PreviousPrime] = total

	return total
}

type NumCache map[int64]int64
type Cache map[int64]NumCache

var numberOfWays = Cache{}

func solve() int64 {

	var i = int64(1)

	for {
		num := Num{
			Value:         i,
			PreviousPrime: i,
		}
		numberOfWays := num.getNumberOfPrimeDivizorChains()
		if numberOfWays >= 5000 {
			return i
		}
		i++
	}

	return 0
}
