package main

import (
	"../mymath"
	"testing"
)

func TestVerifyPrimeNumberReplacingDigits(t *testing.T) {
	//warm up prime numbers cache
	mymath.GetPrimeNumbers(1, 1000000)
	result := VerifyPrimeNumberReplacingDigits(56003, 7)
	if result != true {
		t.Error("Expected true, got", result)
	}
}

func TestVerifyPrimeNumberReplacingDigits2(t *testing.T) {
	//warm up prime numbers cache
	mymath.GetPrimeNumbers(1, 1000000)
	result := VerifyPrimeNumberReplacingDigits(120383, 8)
	if result != false {
		t.Error("Expected false, got", result)
	}
}

func TestVerifyPrimeNumberReplacingDigits3(t *testing.T) {
	//warm up prime numbers cache
	mymath.GetPrimeNumbers(1, 1000000)
	result := verifyPrimeNumberReplacing3Digits(120383, 8)
	if result != false {
		t.Error("Expected false, got", result)
	}
}
