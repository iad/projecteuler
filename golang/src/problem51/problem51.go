package main

import (
	"../mymath"
	"fmt"
	"strconv"
	"time"
)

func verifyPrimeNumberReplacing1Digit(num, targetCount int64) bool {
	str := strconv.Itoa(int(num))
	l := len(str)
	for i := 0; i < l; i++ {
		count := 0
		for d := 0; d < 10; d++ {
			if i == 0 && d == 0 {
				continue
			}
			modifiedStr := str[:i] + strconv.Itoa(d) + str[i+1:]
			modifiedInt, _ := strconv.Atoi(modifiedStr)
			if mymath.IsPrimeNumber(int64(modifiedInt)) {
				// the chain should begin from the number with the same digits
				if int64(modifiedInt) != num && count == 0 {
					continue
				}

				count++
			}
		}
		if int64(count) >= targetCount {
			return true
		}
	}
	return false
}

func verifyPrimeNumberReplacing2Digits(num, targetCount int64) bool {
	str := strconv.Itoa(int(num))
	l := len(str)
	for i := 0; i < l-1; i++ {
		for j := i + 1; j < l; j++ {
			count := 0
			for d := 0; d < 10; d++ {
				if i == 0 && d == 0 {
					continue
				}
				var modifiedStr = str[:i] + strconv.Itoa(d) + str[i+1:]
				modifiedStr = modifiedStr[:j] + strconv.Itoa(d) + modifiedStr[j+1:]
				modifiedInt, _ := strconv.Atoi(modifiedStr)
				if mymath.IsPrimeNumber(int64(modifiedInt)) {
					// the chain should begin from the number with the same digits
					if int64(modifiedInt) != num && count == 0 {
						continue
					}

					count++
				}
			}
			if int64(count) >= targetCount {
				return true
			}
		}
	}
	return false
}

func verifyPrimeNumberReplacing3Digits(num, targetCount int64) bool {
	str := strconv.Itoa(int(num))
	l := len(str)
	for i := 0; i < l-2; i++ {
		for j := i + 1; j < l-1; j++ {
			for k := j + 1; k < l; k++ {
				count := 0
				for d := 0; d < 10; d++ {
					if i == 0 && d == 0 {
						continue
					}
					var modifiedStr = str[:i] + strconv.Itoa(d) + str[i+1:]
					modifiedStr = modifiedStr[:j] + strconv.Itoa(d) + modifiedStr[j+1:]
					modifiedStr = modifiedStr[:k] + strconv.Itoa(d) + modifiedStr[k+1:]
					modifiedInt, _ := strconv.Atoi(modifiedStr)
					if mymath.IsPrimeNumber(int64(modifiedInt)) {
						// the chain should begin from the number with the same digits
						if int64(modifiedInt) != num && count == 0 {
							continue
						}

						count++
					}
				}
				if int64(count) >= targetCount {
					return true
				}
			}
		}
	}
	return false
}

func VerifyPrimeNumberReplacingDigits(num, targetCount int64) bool {
	if verifyPrimeNumberReplacing1Digit(num, targetCount) ||
		verifyPrimeNumberReplacing2Digits(num, targetCount) ||
		verifyPrimeNumberReplacing3Digits(num, targetCount) {
		return true
	}
	return false
}

func main() {
	t0 := time.Now()
	primeNumbers := mymath.GetPrimeNumbers(1, 1000000)

	for _, number := range primeNumbers {
		if VerifyPrimeNumberReplacingDigits(number, 8) {
			fmt.Println(number)
			t1 := time.Now()
			fmt.Printf("The call took %v to run.\n", t1.Sub(t0))
			return
		}
	}

	return
}
