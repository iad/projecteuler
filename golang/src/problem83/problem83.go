package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Graph struct {
	nodes []*Node
}

func (self *Graph) Add(node *Node) {
	self.nodes = append(self.nodes, node)
}

func (self *Graph) FindDistance() int {
	toNodes := make([]*Node, 0)

	for _, node := range self.nodes {
		if node.isFirstNode {
			node.busy = true
			node.TotalValue = node.Value
		}
		if node.isLastNode {
			toNodes = append(toNodes, node)
		}
	}

	for {
		nearestNextNodeDistance := int(0)
		nearestNextNode := &Node{}

		for _, node := range self.nodes {
			if node.busy {
				for _, neigbourNode := range node.links {
					if neigbourNode.busy == false {
						distance := node.TotalValue + neigbourNode.Value
						if distance < nearestNextNodeDistance || nearestNextNodeDistance == 0 {
							nearestNextNodeDistance = distance
							nearestNextNode = neigbourNode
						}
					}
				}
			}

		}

		nearestNextNode.TotalValue = nearestNextNodeDistance
		nearestNextNode.busy = true

		for _, node := range toNodes {
			if node == nearestNextNode {
				return nearestNextNodeDistance
			}
		}
	}
}

type Node struct {
	Value       int
	TotalValue  int
	links       []*Node
	busy        bool
	isFirstNode bool
	isLastNode  bool
}

func (self *Node) Add(node *Node) {
	self.links = append(self.links, node)
}

func readFile(fileName string) ([]string, error) {
	var lines []string

	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println(err)
		return lines, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, nil
}

func parseFileData(lines []string) [][]string {
	data := make([][]string, 0)

	for _, line := range lines {
		data = append(data, parseLine(line))
	}

	return data
}

func parseLine(str string) []string {
	return strings.Split(str, ",")
}

func createGrid(lines [][]string) ([][]*Node, error) {
	graph := make([][]*Node, 0)

	for _, line := range lines {

		graphLine := make([]*Node, 0)

		for _, num := range line {
			numConverted, err := strconv.Atoi(num)
			if err != nil {
				return nil, err
			}

			node := &Node{
				Value: numConverted,
			}

			graphLine = append(graphLine, node)
		}

		graph = append(graph, graphLine)
	}

	return linkGraphNodes(graph), nil
}

func linkGraphNodes(graph [][]*Node) [][]*Node {
	for i, line := range graph {
		for j, node := range line {
			if i > 0 {
				upperNode := graph[i-1][j]
				node.Add(upperNode)
			}
			if i < len(graph)-1 {
				bottomNode := graph[i+1][j]
				node.Add(bottomNode)
			}

			if j > 0 {
				leftNode := graph[i][j-1]
				node.Add(leftNode)
			}
			if j < len(graph[i])-1 {
				rightNode := graph[i][j+1]
				node.Add(rightNode)
			}

			if j == 0 && i == 0 {
				node.isFirstNode = true
			}
		}
	}

	graph[len(graph)-1][len(graph[len(graph)-1])-1].isLastNode = true

	return graph
}

func solve(fileData []string) (int, error) {
	parsedData := parseFileData(fileData)

	grid, err := createGrid(parsedData)
	if err != nil {
		fmt.Print(err)
		return 0, err
	}

	graph := new(Graph)

	for _, line := range grid {
		for _, node := range line {
			graph.Add(node)
		}
	}

	return graph.FindDistance(), nil
}

func main() {
	fileData, err := readFile("p083_matrix.txt")
	if err != nil {
		fmt.Print(err)
		return
	}

	result, err := solve(fileData)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(result)
}
