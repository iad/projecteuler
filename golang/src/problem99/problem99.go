package main

import (
	"bufio"
	"fmt"
	"math/big"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"

	"github.com/davecgh/go-spew/spew"
)

type Task struct {
	LineNumber int64
	Base       int64
	Exponent   int64
}

type Result struct {
	LineNumber int64
	Base       int64
	Exponent   int64
	Result     *big.Int
}

func newManager() *Manager {
	return &Manager{
		leader: Result{
			Result: big.NewInt(0),
		},
	}
}

type Manager struct {
	lock   sync.RWMutex
	leader Result
}

func (m *Manager) CanSkip(t Task) bool {
	m.lock.RLock()
	defer m.lock.RUnlock()

	return t.Base < m.leader.Base && t.Exponent < m.leader.Exponent
}

func (m *Manager) Process(item Result) {
	m.lock.Lock()
	defer m.lock.Unlock()

	leader := m.Get()

	if item.Result.Cmp(leader.Result) == 1 {
		m.leader = item

		fmt.Println("new leader:", item.LineNumber)
	}
}

func (m *Manager) Get() Result {
	return m.leader
}

func main() {
	processorNumbers := runtime.NumCPU()
	tasksCh := make(chan Task, 100)
	resultsCh := make(chan Result, 100)

	fileData, err := readFile("p099_base_exp.txt")
	if err != nil {
		fmt.Print(err)
		return
	}

	manager := newManager()

	go createTasks(fileData, tasksCh)

	go func() {
		// start workers
		wg := &sync.WaitGroup{}
		for i := 0; i <= processorNumbers; i++ {
			wg.Add(1)
			go worker(wg, manager, tasksCh, resultsCh)
		}

		wg.Wait()
		close(resultsCh)
	}()

	processResults(manager, resultsCh)
	result := manager.Get()
	fmt.Println("Result:", result.LineNumber)
}

func createTasks(fileData []string, tasksCh chan<- Task) {
	for i, row := range fileData {
		n1, n2, err := parseLine(row)
		if err != nil {
			panic(err)
		}

		tasksCh <- Task{
			LineNumber: int64(i) + 1,
			Base:       n1,
			Exponent:   n2,
		}
	}

	close(tasksCh)
}

func processResults(manager *Manager, resultsCh <-chan Result) {

	for item := range resultsCh {
		manager.Process(item)
	}
}

func worker(wg *sync.WaitGroup, manager *Manager, tasksCh chan Task, resultsCh chan Result) {
	defer wg.Done()

	for task := range tasksCh {
		if manager.CanSkip(task) {
			continue
		}

		exp := new(big.Int).Exp(big.NewInt(task.Base), big.NewInt(task.Exponent), nil)

		resultsCh <- Result{
			LineNumber: task.LineNumber,
			Base:       task.Base,
			Exponent:   task.Exponent,
			Result:     exp,
		}
	}
}

func readFile(fileName string) ([]string, error) {
	var lines []string

	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println(err)
		return lines, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, nil
}

func parseLine(str string) (int64, int64, error) {
	data := strings.Split(str, ",")
	if len(data) != 2 {
		return 0, 0, fmt.Errorf("expected to have 2 items, but received %v", spew.Sdump(data))
	}

	n1, err := strconv.ParseInt(data[0], 10, 64)
	if err != nil {
		return 0, 0, err
	}

	n2, err := strconv.ParseInt(data[1], 10, 64)
	if err != nil {
		return 0, 0, err
	}

	return n1, n2, nil
}
