package main

import (
	"fmt"
	"reflect"
	"testing"
	"time"
)

func TestIsCorrectNumber(t *testing.T) {
	if result := isCorrectNumber(1345, 4533); result != true {
		t.Error("Expected true", result)
	}
}

func TestIsCorrectNumber2(t *testing.T) {
	if result := isCorrectNumber(1345, 4433); result != false {
		t.Error("Expected false", result)
	}
}

//
//func TestAddToSet(t *testing.T) {
//	set := make([][]int64, 0)
//	set = append(set, []int64{22})
//	set = append(set, []int64{33})
//	set = append(set, []int64{122})
//
//	var add = []int64{2255, 2316, 3377}
//
//	expected := make([][]int64, 0)
//	expected = append(expected, []int64{22, 2255})
//	expected = append(expected, []int64{122, 2255})
//	expected = append(expected, []int64{33, 3377})
//
//	if result := addToSet(set, add); reflect.DeepEqual(expected, result) == false {
//		t.Error("Expected structure got", result)
//	}
//}
//
//func TestAddToSet2(t *testing.T) {
//	set := make([][]int64, 0)
//	set = append(set, []int64{111, 22})
//	set = append(set, []int64{222, 33})
//	set = append(set, []int64{333, 122})
//
//	var add = []int64{2255, 2316, 3377}
//
//	expected := make([][]int64, 0)
//	expected = append(expected, []int64{111, 22, 2255})
//	expected = append(expected, []int64{222, 33, 2255})
//	expected = append(expected, []int64{333, 122, 2255})
//	expected = append(expected, []int64{222, 33, 3377})
//	expected = append(expected, []int64{333, 122, 3377})
//
//	if result := addToSet(set, add); reflect.DeepEqual(expected, result) == false {
//		t.Error("Expected structure got", result)
//	}
//}
//
//func TestAddToSet3(t *testing.T) {
//	set := make([][]int64, 0)
//
//	var add = []int64{2255, 2316, 3377}
//
//	expected := make([][]int64, 0)
//	expected = append(expected, []int64{2255})
//	expected = append(expected, []int64{2316})
//	expected = append(expected, []int64{3377})
//
//	if result := addToSet(set, add); reflect.DeepEqual(expected, result) == false {
//		t.Error("Expected structure got", result)
//	}
//}

func TestIsValidChain(t *testing.T) {
	if result := isValidChain([]int64{1122, 2233, 3344, 4411}); result != true {
		t.Error("Expected true, got", result)
	}
}

func TestIsValidChain2(t *testing.T) {
	if result := isValidChain([]int64{1111, 1111, 1122, 2211}); result != false {
		t.Error("Expected false, got", result)
	}
}

func TestIsValidChain3(t *testing.T) {
	if result := isValidChain([]int64{1122, 4411, 3344, 2233}); result != true {
		t.Error("Expected true, got", result)
	}
}

func TestIsValidChain4(t *testing.T) {
	if result := isValidChain([]int64{7750, 5041, 5017, 4186, 4141, 8640}); result != false {
		t.Error("Expected false, got", result)
	}
}

func TestIsValidChain5(t *testing.T) {
	if result := isValidChain([]int64{8128, 2882, 8281}); result != true {
		t.Error("Expected true, got", result)
	}
}

func TestGetPermutations(t *testing.T) {
	source := make([]List, 0)
	el1 := List{1, 2}
	el2 := List{2, 3}
	el3 := List{3, 4}
	source = append(source, el1, el2, el3)

	expected := make([][]List, 0)
	row1 := []List{el1, el2, el3}
	row2 := []List{el1, el3, el2}
	row3 := []List{el2, el1, el3}
	row4 := []List{el2, el3, el1}
	row5 := []List{el3, el1, el2}
	row6 := []List{el3, el2, el1}
	expected = append(expected, row1, row2, row3, row4, row5, row6)

	if result := getPermutations(source); reflect.DeepEqual(expected, result) == false {
		t.Error("Expected struct, got", result)
	}
}

func Test(t *testing.T) {
	t0 := time.Now()

	result := solve()
	fmt.Println(result)

	t1 := time.Now()
	fmt.Printf("The call took %v to run.\n", t1.Sub(t0))

	//	26033
	//	The call took 1m29.588713395s to run.
}
