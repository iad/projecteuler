package main

import (
	"../mymath"
	"strconv"
)

type List []int64

func getPermutations(chains []List) [][]List {
	out := make([][]List, 0)

	if len(chains) == 1 {
		out = append(out, chains)
		return out
	}

	for key, chain := range chains {
		copyOfChains := make([]List, len(chains))
		copy(copyOfChains, chains)

		newChain := append(copyOfChains[:key], copyOfChains[key+1:]...)

		permutations := getPermutations(newChain)
		for _, permutation := range permutations {
			resultChain := append([]List{chain}, permutation...)
			out = append(out, resultChain)
		}
	}

	return out
}

func isValidChain(chain List) bool {
	countTotal := 0
	for key1, num1 := range chain {
		count := 0
		for key2, num2 := range chain {
			if key1 != key2 && isCorrectNumber(num1, num2) {
				count++
			}
		}
		if count == 1 {
			countTotal++
		}
	}

	if countTotal != len(chain) {
		return false
	}

	// find duplicates
	for key1, num1 := range chain {
		for key2, num2 := range chain {
			if key1 != key2 && num1 == num2 {
				return false
			}
		}
	}

	return true
}

func isCorrectNumber(num1, num2 int64) bool {
	num1End := strconv.Itoa(int(num1))[len(strconv.Itoa(int(num1)))-2:]
	num2Begin := strconv.Itoa(int(num2))[:2]
	return (num1End == num2Begin)
}

func addToSet(chains []List, set List) []List {
	result := make([]List, 0)

	for _, num := range set {
		for _, chain := range chains {

			for _, testNumber := range chain {
				if isCorrectNumber(testNumber, num) {
					copyOfChain := make(List, len(chain))
					copy(copyOfChain, chain)

					newChain := append(copyOfChain, num)
					result = append(result, newChain)
				}
			}

		}
		if len(chains) == 0 {
			result = append(result, List{num})
		}
	}
	return result
}

func solve() int64 {

	numberSets := make([]List, 0)

	numberSets = append(numberSets, mymath.GetTriangleNumbers(1000, 9999))
	numberSets = append(numberSets, mymath.GetSquareNumbers(1000, 9999))
	numberSets = append(numberSets, mymath.GetPentagonalNumbers(1000, 9999))
	numberSets = append(numberSets, mymath.GetHexagonalNumbers(1000, 9999))
	numberSets = append(numberSets, mymath.GetHeptagonalNumbers(1000, 9999))
	numberSets = append(numberSets, mymath.GetOctagonalNumbers(1000, 9999))

	for _, permutation := range getPermutations(numberSets) {
		chains := make([]List, 0)

		for _, numberSet := range permutation {
			chains = addToSet(chains, numberSet)
		}

		for _, chain := range chains {
			if isValidChain(chain) {
				sum := int64(0)
				for _, value := range chain {
					sum += value
				}
				return sum
			}
		}
	}

	return 0
}
