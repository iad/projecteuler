package main

import (
	"fmt"
	"math/big"
	"time"
)

func powBig(a, n int) *big.Int {
	tmp := big.NewInt(int64(a))
	res := big.NewInt(1)
	for n > 0 {
		temp := new(big.Int)
		if n%2 == 1 {
			temp.Mul(res, tmp)
			res = temp
		}
		temp = new(big.Int)
		temp.Mul(tmp, tmp)
		tmp = temp
		n /= 2
	}
	return res
}
func main() {
	t0 := time.Now()

	sum := big.NewInt(0)
	for i := 1; i < 1000; i++ {
		sum = sum.Add(sum, powBig(i, i))
		fmt.Println(i)
	}
	fmt.Println(sum)
	str := sum.String()
	fmt.Println(str[len(str)-10:])
	t1 := time.Now()
	fmt.Printf("The call took %v to run.\n", t1.Sub(t0))
	return
}
