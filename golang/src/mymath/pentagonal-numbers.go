package mymath

func getPentagonalNumber(num int64) int64 {
	return int64(num * (3*num - 1) / 2)
}

func GetPentagonalNumbers(from, to int64) []int64 {
	numbers := []int64{}
	num := int64(1)
	for {
		if triangleNumber := getPentagonalNumber(num); triangleNumber >= from {
			if triangleNumber > to {
				return numbers
			} else {
				numbers = append(numbers, triangleNumber)
			}
		}
		num++
	}
	return numbers
}
