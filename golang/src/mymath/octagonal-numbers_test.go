package mymath

import (
	"reflect"
	"testing"
)

func TestGetOctagonalNumbers(t *testing.T) {
	result := GetOctagonalNumbers(1, 65)
	if reflect.DeepEqual([]int64{1, 8, 21, 40, 65}, result) == false {
		t.Error("Expected [1, 8, 21, 40, 65], got", result)
	}
}
