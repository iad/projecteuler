package mymath

import (
	"reflect"
	"testing"
)

func TestGetTriangleNumbers(t *testing.T) {
	result := GetTriangleNumbers(1, 15)
	if reflect.DeepEqual([]int64{1, 3, 6, 10, 15}, result) == false {
		t.Error("Expected [1, 3, 6, 10, 15], got", result)
	}
}
