package mymath

import (
	"reflect"
	"testing"
)

func TestGetSquareNumbers(t *testing.T) {
	result := GetSquareNumbers(1, 25)
	if reflect.DeepEqual([]int64{1, 4, 9, 16, 25}, result) == false {
		t.Error("Expected [1, 4, 9, 16, 25], got", result)
	}
}
