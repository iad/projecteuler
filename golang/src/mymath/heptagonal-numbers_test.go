package mymath

import (
	"reflect"
	"testing"
)

func TestGetHeptagonalNumbers(t *testing.T) {
	result := GetHeptagonalNumbers(1, 55)
	if reflect.DeepEqual([]int64{1, 7, 18, 34, 55}, result) == false {
		t.Error("Expected [1, 7, 18, 34, 55], got", result)
	}
}
