package mymath

import (
	"math"
)

var knownPrimeNumbersMap = make(map[int64]bool)
var knownPrimeNumbers []int64
var lastKnownPrimeNumber = int64(0)

func GetOddCompositeNumbers(min, max int64) []int64 {

	var notPrimeNumbers = make(map[int64]bool)

	if min < 3 {
		knownPrimeNumbersMap[2] = true
	}

	if min < 3 {
		min = 3
	}
	if min%2 == 0 {
		min++
	}

	for i := 3; int64(i) <= max; i = i + 2 {
		var start = int64(math.Floor(float64(float64(min)/float64(i)) * (float64(i))))
		if start < int64(i*i) {
			start = int64(math.Floor(float64(i * i)))
		}
		for j := start; int64(j) <= max; j = j + int64(i) {
			// mark as not prime
			notPrimeNumbers[j] = true
		}
	}

	var oddCompositeNumbers []int64
	for i := min; i < max; i = i + 2 {
		if _, ok := notPrimeNumbers[i]; ok {
			oddCompositeNumbers = append(oddCompositeNumbers, i)
		} else {
			knownPrimeNumbersMap[i] = true
			knownPrimeNumbers = append(knownPrimeNumbers, int64(i))
		}
	}

	return oddCompositeNumbers
}

func GetPrimeNumbers(min, max int64) []int64 {
	var notPrimeNumbers = make(map[int64]bool)
	var primeNumbers []int64

	if min < 3 {
		primeNumbers = append(primeNumbers, 2)
	}

	if min < 3 {
		min = 3
	}
	if min%2 == 0 {
		min++
	}

	for i := 3; int64(i) <= max; i = i + 2 {
		var start = int64(math.Floor(float64(float64(min)/float64(i)) * (float64(i))))
		if start < int64(i*i) {
			start = int64(math.Floor(float64(i * i)))
		}
		for j := start; int64(j) <= max; j = j + int64(i) {
			// mark as not prime
			notPrimeNumbers[j] = true
		}
	}

	for i := min; i <= max; i = i + 2 {
		if _, ok := notPrimeNumbers[i]; !ok {
			primeNumbers = append(primeNumbers, int64(i))
			knownPrimeNumbersMap[int64(i)] = true
		}
	}

	if lastKnownPrimeNumber < max {
		lastKnownPrimeNumber = max
	}

	return primeNumbers
}

var countOfDiDivisors = make(map[int64]int)

func GetCountOfPrimeDivisors(number int64, primeNumbers []int64) int {
	if count, ok := countOfDiDivisors[number]; ok {
		return int(count)
	}
	dividers := make(map[int64]bool)
	for _, i := range primeNumbers {
		if i > number {
			break
		}

		for j := 1; math.Pow(float64(i), float64(j)) <= float64(number); j++ {
			if number%int64(math.Pow(float64(i), float64(j))) == 0 {
				number = number / int64(math.Pow(float64(i), float64(j)))
				dividers[i] = true
			}
		}
	}

	count := len(dividers)
	countOfDiDivisors[number] = count
	return int(count)
}

func IsPrimeNumber(num int64) bool {
	if lastKnownPrimeNumber < num {
		GetPrimeNumbers(1, num)
	}

	if _, ok := knownPrimeNumbersMap[num]; ok {
		return true
	}
	return false
}

func GetKnownPrimeNumbers() []int64 {
	return knownPrimeNumbers
}
