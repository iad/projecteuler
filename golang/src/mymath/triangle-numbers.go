package mymath

func getTriangleNumber(num int64) int64 {
	return int64(num * (num + 1) / 2)
}

func GetTriangleNumbers(from, to int64) []int64 {
	numbers := []int64{}
	num := int64(1)
	for {
		if triangleNumber := getTriangleNumber(num); triangleNumber >= from {
			if triangleNumber > to {
				return numbers
			} else {
				numbers = append(numbers, triangleNumber)
			}
		}
		num++
	}
	return numbers
}
