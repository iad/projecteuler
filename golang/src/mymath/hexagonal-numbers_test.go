package mymath

import (
	"reflect"
	"testing"
)

func TestGetHexagonalNumbers(t *testing.T) {
	result := GetHexagonalNumbers(1, 45)
	if reflect.DeepEqual([]int64{1, 6, 15, 28, 45}, result) == false {
		t.Error("Expected [1, 6, 15, 28, 45], got", result)
	}
}
