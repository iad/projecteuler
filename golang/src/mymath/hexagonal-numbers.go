package mymath

func getHexagonalNumber(num int64) int64 {
	return int64(num * (2*num - 1))
}

func GetHexagonalNumbers(from, to int64) []int64 {
	numbers := []int64{}
	num := int64(1)
	for {
		if triangleNumber := getHexagonalNumber(num); triangleNumber >= from {
			if triangleNumber > to {
				return numbers
			} else {
				numbers = append(numbers, triangleNumber)
			}
		}
		num++
	}
	return numbers
}
