package mymath

func getHeptagonalNumber(num int64) int64 {
	return int64(num * (5*num - 3) / 2)
}

func GetHeptagonalNumbers(from, to int64) []int64 {
	numbers := []int64{}
	num := int64(1)
	for {
		if triangleNumber := getHeptagonalNumber(num); triangleNumber >= from {
			if triangleNumber > to {
				return numbers
			} else {
				numbers = append(numbers, triangleNumber)
			}
		}
		num++
	}
	return numbers
}
