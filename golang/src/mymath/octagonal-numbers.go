package mymath

func getOctagonalNumber(num int64) int64 {
	return int64(num * (3*num - 2))
}

func GetOctagonalNumbers(from, to int64) []int64 {
	numbers := []int64{}
	num := int64(1)
	for {
		if triangleNumber := getOctagonalNumber(num); triangleNumber >= from {
			if triangleNumber > to {
				return numbers
			} else {
				numbers = append(numbers, triangleNumber)
			}
		}
		num++
	}
	return numbers
}
