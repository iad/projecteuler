package mymath

import (
	"reflect"
	"testing"
)

func TestGetPentagonalNumbers(t *testing.T) {
	result := GetPentagonalNumbers(1, 35)
	if reflect.DeepEqual([]int64{1, 5, 12, 22, 35}, result) == false {
		t.Error("Expected [1, 5, 12, 22, 35], got", result)
	}
}
