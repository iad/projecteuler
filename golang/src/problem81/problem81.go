package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Node struct {
	Value int
	cache int
	links []*Node
}

func (self *Node) Add(node *Node) {
	self.links = append(self.links, node)
}

func (self *Node) GetShortSequenceValue() int {
	if self.cache != 0 {
		return self.cache
	}

	result := int(0)
	for _, node := range self.links {
		nodeValue := node.GetShortSequenceValue()
		if nodeValue < result || result == 0 {
			result = nodeValue
		}
	}

	result += self.Value

	self.cache = result

	return result
}

func readFile(fileName string) ([]string, error) {
	var lines []string

	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println(err)
		return lines, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, nil
}

func parseFileData(lines []string) [][]string {
	data := make([][]string, 0)

	for _, line := range lines {
		data = append(data, parseLine(line))
	}

	return data
}

func parseLine(str string) []string {
	return strings.Split(str, ",")
}

func createGraph(lines [][]string) ([][]*Node, error) {
	graph := make([][]*Node, 0)

	for _, line := range lines {

		graphLine := make([]*Node, 0)

		for _, num := range line {
			numConverted, err := strconv.Atoi(num)
			if err != nil {
				return nil, err
			}

			node := &Node{
				Value: numConverted,
			}

			graphLine = append(graphLine, node)
		}

		graph = append(graph, graphLine)
	}

	return linkGraphNodes(graph), nil
}

func linkGraphNodes(graph [][]*Node) [][]*Node {
	for i, line := range graph {
		for j, node := range line {
			if i > 0 {
				upperNode := graph[i-1][j]
				node.Add(upperNode)
			}
			if j > 0 {
				leftNode := graph[i][j-1]
				node.Add(leftNode)
			}
		}
	}

	return graph
}

func getLastNode(g [][]*Node) *Node {
	lastLine := g[len(g)-1]
	lastNode := lastLine[len(lastLine)-1]
	return lastNode
}

func solve(fileData []string) (int, error) {
	parsedData := parseFileData(fileData)

	graph, err := createGraph(parsedData)
	if err != nil {
		fmt.Print(err)
		return 0, err
	}

	node := getLastNode(graph)

	return node.GetShortSequenceValue(), err
}

func main() {
	fileData, err := readFile("p081_matrix.txt")
	if err != nil {
		fmt.Print(err)
		return
	}

	result, err := solve(fileData)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(result)
}
