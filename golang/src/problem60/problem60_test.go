package main

import (
	//	"fmt"
	"testing"
	//	"time"
	"../mymath"
	"fmt"
	"time"
)

func TestIsMagicPear(t *testing.T) {
	// warm up prime numbers
	mymath.GetPrimeNumbers(1, 10000*10000)

	if result := isMagicPear(3, 7); result != true {
		t.Error("Expected true, got", result)
	}
}

func TestIsMagicPear2(t *testing.T) {
	if result := isMagicPear(7, 10); result != false {
		t.Error("Expected false, got", result)
	}
}

func TestIsMagicSet1(t *testing.T) {
	if result := isMagicSet([]int64{3, 7, 109, 673}); result != true {
		t.Error("Expected true, got", result)
	}
}

func TestIsMagicSet2(t *testing.T) {
	if result := isMagicSet([]int64{3, 7, 109, 674}); result != false {
		t.Error("Expected false, got", result)
	}
}

func TestIsMagicSet3(t *testing.T) {
	// warm up cache
	mymath.GetPrimeNumbers(1, 8500*8500)
	if result := isMagicSet([]int64{13, 5197, 5701, 6733, 8389}); result != true {
		t.Error("Expected true, got", result)
	}
}

func TestSolve(t *testing.T) {
	if result := solve(1000, 4); result != 792 {
		t.Error("Expected 792, got", result)
	}
}

func Test(t *testing.T) {
	t0 := time.Now()

	result := solve(10000, 5)
	fmt.Println(result)

	t1 := time.Now()
	fmt.Printf("The call took %v to run.\n", t1.Sub(t0))

	//	26033
	//	The call took 1m29.588713395s to run.
}
