package main

import (
	"../mymath"
	"strconv"
)

func numInSlice(a int64, list []int64) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func sum(a []int64) (s int64) {
	for _, v := range a {
		s += v
	}
	return
}

func isMagicSet(set []int64) bool {
	for pos, primeNumber1 := range set {
		for _, primeNumber2 := range set[pos+1:] {
			if !isMagicPear(primeNumber1, primeNumber2) {
				return false
			}
		}
	}
	return true
}

func isMagicPear(num1, num2 int64) bool {
	num1String := strconv.Itoa(int(num1))
	num2String := strconv.Itoa(int(num2))
	concatNum1, _ := strconv.Atoi(num1String + num2String)
	concatNum2, _ := strconv.Atoi(num2String + num1String)
	concatNum1Int64 := int64(concatNum1)
	concatNum2Int64 := int64(concatNum2)
	return (mymath.IsPrimeNumber(concatNum1Int64) && mymath.IsPrimeNumber(concatNum2Int64))
}

func solve(max, deep int64) int64 {
	chains := make([][]int64, 0)

	//warm up cache
	mymath.GetPrimeNumbers(1, int64(max*max))

	primeNumbers := mymath.GetPrimeNumbers(1, max)
	for _, primeNumber := range primeNumbers {
		newChains := make([][]int64, 0)

		for _, chain := range chains {

			copyOfChain := make([]int64, len(chain))
			copy(copyOfChain, chain)
			newChain := append(copyOfChain, int64(primeNumber))

			if isMagicSet(newChain) {
				newChains = append(newChains, newChain)
				if int64(len(newChain)) >= deep {
					return sum(newChain)
				}
			}
		}
		newChains = append(newChains, []int64{int64(primeNumber)})
		chains = append(chains, newChains...)
	}

	return 0
}

func main() {
	return
}
