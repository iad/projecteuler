package main

import (
	"fmt"
	"testing"
	"time"
)

func TestSampleCase(t *testing.T) {
	data := []string{
		"131,673,234,103,18",
		"201,96,342,965,150",
		"630,803,746,422,111",
		"537,699,497,121,956",
		"805,732,524,37,331",
	}

	if result, _ := solve(data); result != 994 {
		t.Error("Expected 994, got ", result)
	}
}

func Test(t *testing.T) {
	t0 := time.Now()

	main()

	t1 := time.Now()
	fmt.Printf("The call took %v to run.\n", t1.Sub(t0))

	//260324
	//The call took 24.599538ms to run.
}
