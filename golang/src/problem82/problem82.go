package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Node struct {
	Value       int
	cache       int
	links       []*Node
	busy        bool
	isFirstNode bool
}

func (self *Node) Add(node *Node) {
	self.links = append(self.links, node)
}

func (self *Node) GetShortSequenceValue() int {
	shortValue := self.getShortSequenceValueRecursive()

	self.cache = shortValue

	return shortValue
}

func (self *Node) getShortSequenceValueRecursive() int {
	if self.cache != 0 {
		return self.cache
	}

	if self.isFirstNode {
		self.cache = self.Value
		return self.cache
	}

	// lock node to prevent recursions
	self.busy = true

	result := int(0)
	for _, node := range self.links {
		if node.busy == false {
			nodeValue := node.getShortSequenceValueRecursive()
			if nodeValue < result || result == 0 {
				result = nodeValue
			}
		}
	}

	result += self.Value

	// unlock node
	self.busy = false

	return result
}

func readFile(fileName string) ([]string, error) {
	var lines []string

	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println(err)
		return lines, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, nil
}

func parseFileData(lines []string) [][]string {
	data := make([][]string, 0)

	for _, line := range lines {
		data = append(data, parseLine(line))
	}

	return data
}

func parseLine(str string) []string {
	return strings.Split(str, ",")
}

func createGraph(lines [][]string) ([][]*Node, error) {
	graph := make([][]*Node, 0)

	for _, line := range lines {

		graphLine := make([]*Node, 0)

		for _, num := range line {
			numConverted, err := strconv.Atoi(num)
			if err != nil {
				return nil, err
			}

			node := &Node{
				Value: numConverted,
			}

			graphLine = append(graphLine, node)
		}

		graph = append(graph, graphLine)
	}

	return linkGraphNodes(graph), nil
}

func linkGraphNodes(graph [][]*Node) [][]*Node {
	for i, line := range graph {
		for j, node := range line {
			if i > 0 {
				upperNode := graph[i-1][j]
				node.Add(upperNode)
			}
			if j > 0 {
				leftNode := graph[i][j-1]
				node.Add(leftNode)
			}
			if i < len(graph)-1 {
				bottomNode := graph[i+1][j]
				node.Add(bottomNode)
			}
			if j == 0 {
				node.isFirstNode = true
			}
		}
	}

	return graph
}

func getLastNodes(g [][]*Node) []*Node {
	nodes := make([]*Node, 0)
	for _, line := range g {
		nodes = append(nodes, line[len(line)-1])
	}

	return nodes
}

func findShortestNode(nodes []*Node) *Node {
	resultNode := &Node{}
	for _, node := range nodes {
		shortestSequenceValue := node.GetShortSequenceValue()
		if shortestSequenceValue < resultNode.GetShortSequenceValue() || resultNode.GetShortSequenceValue() == 0 {
			resultNode = node
		}
	}
	return resultNode
}

func warmUpCache(g [][]*Node) {
	line := g[0]
	for j, _ := range line {
		for _, line := range g {
			line[j].GetShortSequenceValue()
		}
	}
}

func solve(fileData []string) (int, error) {
	parsedData := parseFileData(fileData)

	graph, err := createGraph(parsedData)
	if err != nil {
		fmt.Print(err)
		return 0, err
	}

	warmUpCache(graph)

	nodes := getLastNodes(graph)

	node := findShortestNode(nodes)

	return node.GetShortSequenceValue(), err
}

func main() {
	fileData, err := readFile("p082_matrix.txt")
	if err != nil {
		fmt.Print(err)
		return
	}

	result, err := solve(fileData)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(result)
}
