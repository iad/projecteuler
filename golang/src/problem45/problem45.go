package main

import "fmt"
import "time"

var triangleNumbers = make(map[int64]int64)
var hexagonalNumbers = make(map[int64]int64)
var pentagonNumbers = make(map[int64]int64)

var knownTriangleNumbers = make(map[int64]bool)
var knownHexagonalNumbers = make(map[int64]bool)
var knownPentagonNumbers = make(map[int64]bool)

func GetTriangleNumber(n int64) (value int64) {
	if v, ok := triangleNumbers[n]; ok {
		value = v
	} else {
		value = n * (n + 1) / 2
		triangleNumbers[n] = value
		knownTriangleNumbers[value] = true
	}
	return
}

func IsTriangleNumber(value int64) bool {
	if _, ok := knownTriangleNumbers[value]; ok {
		return true
	}
	return false
}

func GetHexagonalNumber(n int64) (value int64) {
	if v, ok := hexagonalNumbers[n]; ok {
		value = v
	} else {
		value = n * (2*n - 1)
		hexagonalNumbers[n] = value
		knownHexagonalNumbers[value] = true
	}
	return
}

func IsHexagonalNumber(value int64) bool {
	if _, ok := knownHexagonalNumbers[value]; ok {
		return true
	}
	return false
}

func GetPentagonNumber(n int64) (value int64) {
	if v, ok := pentagonNumbers[n]; ok {
		value = int64(v)
	} else {
		value = n * (3*n - 1) / 2
		pentagonNumbers[n] = value
		knownPentagonNumbers[value] = true
	}
	return
}

func IsPentagonNumber(value int64) bool {
	if _, ok := knownPentagonNumbers[value]; ok {
		return true
	}
	return false
}

func main() {
	t0 := time.Now()
	for i := 1; i < 1000000; i++ {
		triangleNum := GetTriangleNumber(int64(i))
		GetHexagonalNumber(int64(i))
		GetPentagonNumber(int64(i))
		if IsHexagonalNumber(triangleNum) {
			if IsPentagonNumber(triangleNum) {
				if triangleNum > 40755 {
					fmt.Println(triangleNum)
					t1 := time.Now()
					fmt.Printf("The call took %v to run.\n", t1.Sub(t0))
					return
				}
			}
		}
	}
	return
}
