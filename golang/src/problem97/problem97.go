package main

import (
	"fmt"
	"math/big"
)

func main() {
	n1 := big.NewInt(28433)
	n2 := big.NewInt(7830457)

	exp := new(big.Int).Exp(big.NewInt(2), n2, nil)

	mul := new(big.Int).Mul(n1, exp)
	sum := new(big.Int).Add(mul, big.NewInt(1))
	result := sum.String()

	fmt.Println("result:", result[len(result)-10:])
}
