package main

import (
	"../mymath"
	"fmt"
	"math"
	"time"
)

func checkNumber(num int64, primeNumbers []int64) bool {
	for _, primeNumber := range primeNumbers {
		for i := 1; float64(i) < math.Sqrt(float64(num)); i++ {
			if int64(primeNumber+2*(int64(i)*int64(i))) == num {
				return false
			}
		}
	}
	return true
}

func main() {
	t0 := time.Now()
	oddCompositeNumbers := mymath.GetOddCompositeNumbers(1, 1000000)
	var primeNumbers = mymath.GetKnownPrimeNumbers()
	for _, number := range oddCompositeNumbers {

		if isSpecial := checkNumber(number, primeNumbers); isSpecial {
			fmt.Println(number)
			t1 := time.Now()
			fmt.Printf("The call took %v to run.\n", t1.Sub(t0))
			return
		}
	}
}
